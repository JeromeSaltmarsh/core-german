import 'package:coregerman/model/core_german.dart';
import 'package:coregerman/ui/configuration.dart';
import 'package:coregerman/ui/core_german_ui.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MainApp());

class MainApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    print("MainApp.build()");

    return MultiProvider(
        providers: [
          ChangeNotifierProvider<CoreGerman>.value(
            value: CoreGerman(),
          ),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: "German By Doing",
          theme: UIConfiguration.theme,
          home: CoreGermanUI(),
        ));
  }
}
