import 'package:coregerman/model/entities/sentence.dart';
import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/model/repositories/memory_repository.dart';

class SentenceValue {
  String text;
  String wordId;
  Sentence sentence;

  SentenceValue({this.text, this.wordId});

  Word get word {
    if (hasWord) {
      return MemoryRepository.findWordById(wordId);
    }
    return null;
  }

  bool get hasWord {
    return wordId != null;
  }

  void save() {
    sentence.save();
  }
}
