import 'package:coregerman/model/enums/case.dart';
import 'package:coregerman/model/enums/difficulty.dart';
import 'package:coregerman/model/enums/gender.dart';
import 'package:coregerman/model/enums/modal_verbs.dart';
import 'package:coregerman/model/enums/pronouns.dart';
import 'package:coregerman/model/enums/sub_word_type.dart';
import 'package:coregerman/model/enums/tense.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/model/repositories/firestore_repository.dart';
import 'package:flutter/foundation.dart';

class Word {
  final String id;
  String value;
  WordType type;
  Gender gender;
  Case kase;
  String english;
  Difficulty difficulty;
  ModalVerb modalVerb;
  Pronoun pronoun;
  Tense tense;
  SubWordType subWordType;

  Word(this.value,
      {@required this.type,
      this.gender,
      this.kase,
      this.id,
      this.english,
      this.difficulty,
      this.modalVerb,
      this.pronoun,
      this.tense,
      this.subWordType});

  Map<String, dynamic> toJson() {
    return {
      'value': value,
      'type': type?.toString(),
      'gender': gender?.toString(),
      'case': kase?.toString(),
      'difficulty': difficulty?.toString(),
      'english': english,
      'modalVerb': modalVerb?.toString(),
      'pronoun': pronoun?.toString(),
      'tense': tense?.toString(),
      'subWordType' : subWordType?.toString(),
    };
  }

  void save(){
    FirestoreRepository.saveWord(this);
  }
}
