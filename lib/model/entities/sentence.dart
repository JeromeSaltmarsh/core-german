import 'package:coregerman/model/entities/sentence_value.dart';
import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/model/repositories/firestore_repository.dart';
import 'package:flutter/foundation.dart';

@immutable
class Sentence {
  final String id;
  String english;
  bool isQuestion;
  List<SentenceValue> sentenceValues;

  Sentence({this.id, this.sentenceValues, this.english, this.isQuestion}){
    sentenceValues.forEach((sentenceValue) => sentenceValue.sentence = this);
  }

  bool contains(Word word) {
    return word != null &&
        sentenceValues.any((element) => element.wordId == word.id);
  }

  List<String> get wordIds => sentenceValues
      .where((sentenceValue) => sentenceValue.wordId != null)
      .map((sentenceValue) => sentenceValue.wordId)
      .toList();

  String get text{
    if(isQuestion){
      return sentenceValues.fold("", (previousValue, sentenceValue) => "$previousValue ${sentenceValue.text}") + "?";
    }
    return sentenceValues.fold("", (previousValue, sentenceValue) => "$previousValue ${sentenceValue.text}");
  }

  void save() {
    FirestoreRepository.saveSentence(this);
  }
}
