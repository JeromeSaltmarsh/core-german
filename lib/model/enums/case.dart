
enum Case {
  Nominative,
  Accusative,
  Dative,
  Two_Way,
  Genitive
}

class CaseUtil{

  static String getDescription(Case kase){
    switch(kase){
      case Case.Nominative:
        return "The subject of a sentence";
      case Case.Accusative:
        return "The direct object";
      case Case.Dative:
        return "The indirect object";
      case Case.Genitive:
        return "Commonly used for showing possession";
      default:
        throw Exception("Case missing");
    }
  }
}