
import 'package:coregerman/model/enums/sub_word_type.dart';

enum ModalVerb{
  Duerfen,
  Koennen,
  Muessen,
  Sollen,
  Wollen,
  Moegen,
  Moechten,
}

extension ModalVerbExtensions on ModalVerb{

  SubWordType get subWordType{
    switch(this){
      case ModalVerb.Duerfen:
        return SubWordType.ModalVerb_Duerfen;
      case ModalVerb.Koennen:
        return SubWordType.ModalVerb_Koennen;
      case ModalVerb.Muessen:
        return SubWordType.ModalVerb_Muessen;
      case ModalVerb.Sollen:
        return SubWordType.ModalVerb_Sollen;
      case ModalVerb.Wollen:
        return SubWordType.ModalVerb_Wollen;
      case ModalVerb.Moegen:
        return SubWordType.ModalVerb_Moegen;
      case ModalVerb.Moechten:
        return SubWordType.ModalVerb_Moechten;
  }
  throw Exception("$this Unmapped");
}
}
