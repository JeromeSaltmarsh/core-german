enum Difficulty { Beginner, Intermediate, Expert }

extension DifficultyExtensions on Difficulty {

  int get toInt {
    switch (this) {
      case Difficulty.Beginner:
        return 1;
      case Difficulty.Intermediate:
        return 2;
      case Difficulty.Expert:
        return 3;
    }
    return 1;
  }
}
