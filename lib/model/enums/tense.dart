enum Tense {
  Past,
  Present,
  Future,
}

extension TenseExtensions on Tense {
  String get description {
    switch (this) {
      case Tense.Past:
        return "Präteritum";
      case Tense.Present:
        return "Präsens";
      case Tense.Future:
        return "Konjunktiv II";
    }
    throw Exception("No Description available for $this");
  }
}
