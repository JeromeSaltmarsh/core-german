
import 'package:coregerman/model/enums/modal_verbs.dart';
import 'package:coregerman/model/enums/word_type.dart';

enum SubWordType{

  Article_Definite,
  Article_Indefinite,

  ModalVerb_Duerfen,
  ModalVerb_Koennen,
  ModalVerb_Wollen,
  ModalVerb_Muessen,
  ModalVerb_Sollen,
  ModalVerb_Moegen,
  ModalVerb_Moechten,

  Pronoun_Possessive,
  Pronoun_Personal,

  Keyword_Sein,
  Keyword_Haben,
  Keyword_Werden,

  Keyword_Machen,
  Keyword_Gewesen,
  Keyword_Waere,
  Keyword_Haette,
  Keyword_Wuerde,
}

extension SubWordTypeExtensions on SubWordType{

  static List<SubWordType>  _hasTense = [
    SubWordType.ModalVerb_Koennen,
    SubWordType.Keyword_Sein,
    SubWordType.Keyword_Haben,
    SubWordType.Keyword_Werden,
    SubWordType.Keyword_Machen
  ];

  bool get hasTense{
    return _hasTense.contains(this);
  }

  ModalVerb get modalVerb{
    switch(this){
      case SubWordType.ModalVerb_Koennen:
        return ModalVerb.Koennen;
      case SubWordType.ModalVerb_Wollen:
        return ModalVerb.Wollen;
      case SubWordType.ModalVerb_Muessen:
        return ModalVerb.Muessen;
      case SubWordType.ModalVerb_Sollen:
        return ModalVerb.Sollen;
      case SubWordType.ModalVerb_Moegen:
        return ModalVerb.Moegen;
      case SubWordType.ModalVerb_Moechten:
        return ModalVerb.Moechten;
      default:
        throw Exception("$this is not a modal verb");
    }
  }

  bool get isModalVerb{
    switch(this){
      case SubWordType.ModalVerb_Koennen:
        return true;
      case SubWordType.ModalVerb_Wollen:
        return true;
      case SubWordType.ModalVerb_Muessen:
        return true;
      case SubWordType.ModalVerb_Sollen:
        return true;
      case SubWordType.ModalVerb_Moegen:
        return true;
      case SubWordType.ModalVerb_Moechten:
        return true;
      default:
        return false;
    }
  }

   WordType get wordType {
    switch(this){
      case SubWordType.Article_Definite:
        return WordType.Definite_Article;
      case SubWordType.Article_Indefinite:
        return WordType.Indefinite_Article;
      case SubWordType.ModalVerb_Duerfen:
        return WordType.Modal_Verb;
      case SubWordType.ModalVerb_Koennen:
        return WordType.Modal_Verb;
      case SubWordType.ModalVerb_Moegen:
        return WordType.Modal_Verb;
      case SubWordType.ModalVerb_Muessen:
        return WordType.Modal_Verb;
      case SubWordType.ModalVerb_Wollen:
        return WordType.Modal_Verb;
      case SubWordType.ModalVerb_Sollen:
        return WordType.Modal_Verb;
      case SubWordType.ModalVerb_Moechten:
        return WordType.Modal_Verb;
      case SubWordType.Pronoun_Possessive:
        return WordType.Pronoun;
      case SubWordType.Pronoun_Personal:
        return WordType.Pronoun;
      case SubWordType.Keyword_Sein:
        return WordType.Keyword;
      case SubWordType.Keyword_Haben:
        return WordType.Keyword;
      case SubWordType.Keyword_Werden:
        return WordType.Keyword;
      case SubWordType.Keyword_Machen:
        return WordType.Keyword;
      case SubWordType.Keyword_Gewesen:
        return WordType.Keyword;
      case SubWordType.Keyword_Waere:
        return WordType.Keyword;
      case SubWordType.Keyword_Haette:
        return WordType.Keyword;
      case SubWordType.Keyword_Wuerde:
        return WordType.Keyword;
      case SubWordType.Keyword_Machen:
        return WordType.Keyword;
    }
    throw Exception("Unknown WordType");
  }
}

