import 'package:coregerman/model/enums/difficulty.dart';

enum WordType {
  Keyword,
  Definite_Article,
  Indefinite_Article,
  Noun,
  Verb,
  Modal_Verb,
  Pronoun,
  Question,
  Adjective,
  Preposition,
  Adverb,
  Conjunction,
  Unmapped,
//  Overview,
}

List<WordType> _gendered = [
  WordType.Definite_Article,
  WordType.Indefinite_Article,
  WordType.Noun,
];

List<WordType> _hasCase = [
  WordType.Definite_Article,
  WordType.Indefinite_Article,
  WordType.Verb,
  WordType.Pronoun,
  WordType.Preposition,
];

List<WordType> _hasTense = [
  WordType.Modal_Verb,
  WordType.Keyword,
];

List<WordType> _hasPronoun = [
  WordType.Modal_Verb,
  WordType.Pronoun,
];

List<WordType> _hasSubWordType = [
  WordType.Keyword, WordType.Modal_Verb
];

extension WordTypeExtensions on WordType {

  bool get hasGender {
    return _gendered.any((element) => element == this);
  }

  bool get hasCase {
    return _hasCase.any((element) => element == this);
  }

  bool get hasSubWordType{
    return _hasSubWordType.any((element) => element == this);
  }

  bool get hasTense {
    return _hasTense.any((element) => element == this);
  }

  bool get hasModalVerb {
    return this == WordType.Modal_Verb;
  }

  bool get hasPronoun {
    return _hasPronoun.any((element) => element == this);
  }

  Difficulty get difficulty {
    switch (this) {
      case WordType.Question:
        return Difficulty.Intermediate;
      case WordType.Adjective:
        return Difficulty.Intermediate;
      case WordType.Preposition:
        return Difficulty.Expert;
      default:
        return Difficulty.Beginner;
    }
  }

  String get text{
    switch(this){
      case WordType.Keyword:
        return "Keywords";
      case WordType.Definite_Article:
        return "Definite Articles";
      case WordType.Indefinite_Article:
        return "Indefinite Articles";
      case WordType.Noun:
        return "Nouns";
      case WordType.Verb:
        return "Verbs";
      case WordType.Modal_Verb:
        return "Modal-Verbs";
      case WordType.Pronoun:
        return "Pronouns";
      case WordType.Question:
        return "Questions";
      case WordType.Adjective:
        return "Adjectives";
      case WordType.Preposition:
        return "Prepositions";
      case WordType.Adverb:
        return "Adverbs";
      case WordType.Unmapped:
        return "Unmapped";
      case WordType.Conjunction:
        return "Conjunctions";
      default:
        this.toString();
    }
  }
}
