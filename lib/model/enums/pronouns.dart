enum Pronoun{
  Ich,
  Du,
  Er_Sie_Es,
  Wir,
  Ihr,
  Sie_They,

  Er,
  Sie_She,
  Es,
}

extension PronounExtensions on Pronoun{

  bool get isThirdPersonSingular{
    return this != Pronoun.Er && this != Pronoun.Sie_She && this != Pronoun.Es;
  }
}