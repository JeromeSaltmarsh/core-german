import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coregerman/model/entities/sentence.dart';
import 'package:coregerman/model/entities/sentence_value.dart';
import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/model/enums/case.dart';
import 'package:coregerman/model/enums/difficulty.dart';
import 'package:coregerman/model/enums/gender.dart';
import 'package:coregerman/model/enums/modal_verbs.dart';
import 'package:coregerman/model/enums/pronouns.dart';
import 'package:coregerman/model/enums/sub_word_type.dart';
import 'package:coregerman/model/enums/tense.dart';
import 'package:coregerman/model/enums/word_type.dart';

class FirestoreRepository {

  static CollectionReference _words = _firestore.collection('words');
  static CollectionReference _sentences = _firestore.collection('sentences');
  static Firestore get _firestore => Firestore.instance;

  static Future<List<Word>> getAllWords() async {
    QuerySnapshot allWordsSnapshot = await _words.getDocuments();
    return allWordsSnapshot.documents.map(_toWord).toList();
  }

  static Future<List<Sentence>> getAllSentences() async {
    QuerySnapshot allSentencesSnapshot = await _sentences.getDocuments();
    return allSentencesSnapshot.documents.map(_toSentence).toList();
  }

  static Sentence _toSentence(DocumentSnapshot snapshot) {
    Map<String, dynamic> data = snapshot.data;
    var sentenceValueObjects = data['sentenceValues'];
    var sentenceValues = sentenceValueObjects.map((sentenceValue) {
      return SentenceValue(text: sentenceValue['text'], wordId: sentenceValue['wordId']);
    }).toList();
    return Sentence(
      id: snapshot.documentID,
      english: data['english'],
      isQuestion: data['isQuestion'] ?? false,
      sentenceValues: [...sentenceValues],
    );
  }

  static Word _toWord(DocumentSnapshot snapshot) {
    Map<String, dynamic> data = snapshot.data;
    return Word(data['value'],
        id: snapshot.documentID,
        english: data['english'],
        type: parse(data['type'], WordType.values),
        gender: parse(data['gender'], Gender.values),
        kase: parse(data['case'], Case.values),
        difficulty: parse(data['difficulty'], Difficulty.values),
        modalVerb: parse(data['modalVerb'], ModalVerb.values),
        pronoun: parse(data['pronoun'], Pronoun.values),
        tense: parse(data['tense'], Tense.values),
        subWordType: parse(data['subWordType'], SubWordType.values),
    );
  }

  static Future<void> saveSentence(Sentence sentence,
      {bool merge = true}) async {
    print("FirestoreRepository.saveSentence(${sentence.id})");
    return _sentences
        .document(sentence.id)
        .setData(sentenceToJson(sentence), merge: merge);
  }

  static Future<void> saveWord(Word word, {bool merge = true}) async {
    Map<String, dynamic> wordJson = word.toJson();
    print("FirestoreRepository.saveWord($wordJson)");
    return _words.document(word.id).setData(wordJson, merge: true);
  }



  static Future deleteSentence(Sentence sentence) async {
    print("FirestoreRepository.deleteSentence(${sentence.id})");
    await _firestore.runTransaction((Transaction myTransaction) async {
      await myTransaction.delete(_sentences.document(sentence.id));
    });
  }

  static Future deleteWord(Word word) async {
    print("FirestoreRepository.deleteWord(${word.id})");
    await _firestore.runTransaction((Transaction myTransaction) async {
      await myTransaction.delete(_words.document(word.id));
    });
  }

  // Utility Functions

  // TODO move to sentence class
  static Map<String, dynamic> sentenceToJson(Sentence sentence) {
    return {
      'english': sentence.english,
      'isQuestion': sentence.isQuestion,
      'sentenceValues': sentence.sentenceValues.map((e) => {
            'text': e.text,
            'wordId': e.wordId,
          }),
    };
  }

  static T returnNull<T>() {
    return null;
  }

  static T parse<T>(String text, List<T> values) {
    if (text == null) {
      return null;
    }
    return values.firstWhere((element) => element.toString() == text,
        orElse: returnNull);
  }
}
