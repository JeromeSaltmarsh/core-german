import 'package:coregerman/model/entities/sentence.dart';
import 'package:coregerman/model/entities/sentence_value.dart';
import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/model/enums/case.dart';
import 'package:coregerman/model/enums/difficulty.dart';
import 'package:coregerman/model/enums/gender.dart';
import 'package:coregerman/model/enums/modal_verbs.dart';
import 'package:coregerman/model/enums/pronouns.dart';
import 'package:coregerman/model/enums/sub_word_type.dart';
import 'package:coregerman/model/enums/tense.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:flutter/foundation.dart';
import 'package:uuid/uuid.dart';

class MemoryRepository {
  static List<Word> words;
  static List<Sentence> sentences;
  static Uuid uuid = Uuid();

  // Words API

  static Word findWordById(String id) {
    if (id == null) {
      return null;
    }
    return words.firstWhere((word) => word.id == id, orElse: returnNull);
  }

  static Sentence fetchParentSentence(SentenceValue sentenceValue) {
    print("MemoryRepository.fetchParentSentence(${sentenceValue.text})");
    return sentences.firstWhere(
        (element) => element.sentenceValues.contains(sentenceValue));
  }

  static List<Sentence> getRandomAnswers({int max = 20}) {
    print("MemoryRepository.getRandomAnswers()");
    sentences.shuffle();
    return sentences.sublist(
        0, max > sentences.length ? sentences.length - 1 : max - 1);
  }

  static List<Word> getWords(List<String> wordIds) {
    return wordIds.map(findWordById).toList();
  }

  static Sentence findSentenceContaining({Word word}) {
    return sentences.firstWhere(
        (sentence) => sentence.wordIds.contains(word.id),
        orElse: returnNull);
  }

  static List<Sentence> findSentencesContaining(
      {Word word, WordType wordType, Case kase, SubWordType subWordType}) {
    if (word != null) {
      return sentences
          .where((sentence) => sentence.wordIds.contains(word.id))
          .toList();
    }
    if (wordType != null) {
      return sentences
          .where((sentence) => getWords(sentence.wordIds)
              .any((element) => element.type == wordType))
          .toList();
    }
    if (kase != null) {
      return sentences
          .where((sentence) =>
              getWords(sentence.wordIds).any((element) => element.kase == kase))
          .toList();
    }
    if (subWordType != null) {
      Word word = MemoryRepository.findWord(
          wordType: subWordType.wordType,
          subWordType: subWordType,
          condition: (word) => word.tense == null);
      if (word != null) {
        return findSentencesContaining(word: word);
      }
    }
    return sentences;
  }

  static Sentence findFirstAnswerContaining(Word word) {
    return sentences.firstWhere(
        (sentence) => sentence.wordIds.contains(word.id),
        orElse: returnNull);
  }

  static Word findWord({
    WordType wordType,
    Gender gender,
    Case kase,
    String value,
    String id,
    ModalVerb modalVerb,
    Pronoun pronoun,
    Tense tense,
    SubWordType subWordType,
    bool condition(Word word),
  }) {
    return words.firstWhere((word) {
      if (id != null && word.id != id) {
        return false;
      }
      if (wordType != null && word.type != wordType) {
        return false;
      }
      if (gender != null && word.gender != gender) {
        return false;
      }
      if (kase != null && word.kase != kase) {
        return false;
      }
      if (value != null && word.value != value) {
        return false;
      }
      if (modalVerb != null && word.modalVerb != modalVerb) {
        return false;
      }
      if (pronoun != null && word.pronoun != pronoun) {
        if (wordType == modalVerb) {
          if (pronoun == Pronoun.Er ||
              pronoun == Pronoun.Sie_She ||
              pronoun == Pronoun.Es) {}
        }
        return false;
      }
      if (tense != null && word.tense != tense) {
        return false;
      }
      if (subWordType != null && subWordType != word.subWordType) {
        return false;
      }
      if (condition != null && !condition(word)) {
        return false;
      }
      return true;
    }, orElse: () {
      return null;
    });
  }

  static List<Word> findWords(
      {WordType wordType,
      Gender gender,
      Case kase,
      String value,
      String id,
      Pronoun pronoun}) {
    return words.where((word) {
      if (id != null && word.id != id) {
        return false;
      }
      if (pronoun != null && word.pronoun != pronoun) {
        return false;
      }
      if (wordType != null && word.type != wordType) {
        return false;
      }
      if (gender != null && word.gender != gender) {
        return false;
      }
      if (kase != null && word.kase != kase) {
        return false;
      }
      if (value != null && word.value.toLowerCase() != value.toLowerCase()) {
        return false;
      }
      return true;
    }).toList();
  }

  static List<Sentence> searchSentences(String query) {
    print("MemoryRepository.searchSentences($query)");

    if (query == null || query.isEmpty) {
      return sentences;
    }

    String queryLower = query.toLowerCase();
    return sentences
        .where((sentence) =>
            sentence.text.toLowerCase().contains(queryLower) ||
            (sentence.english != null &&
                sentence.english.toLowerCase().contains(queryLower)))
        .toList();
  }

  static List<Word> searchWords(String query) {
    if (query == null || query.isEmpty) {
      return [];
    }
    return words
        .where((word) => word.value.toLowerCase().contains(query.toLowerCase()))
        .toList();
  }

  static Word addWord(
    String value, {
    @required WordType wordType,
    Gender gender,
    Case kase,
    String english,
    ModalVerb modalVerb,
    Pronoun pronoun,
    Tense tense,
    Difficulty difficulty,
    SubWordType subWordType,
  }) {
    print("MemoryRepository.addWord($value)");

    Word word = Word(value,
        type: wordType,
        gender: gender,
        kase: kase,
        id: _generateId(),
        english: english,
        modalVerb: modalVerb,
        pronoun: pronoun,
        tense: tense,
        difficulty: difficulty,
        subWordType: subWordType);

    print("MemoryRepository word created id:${word.id}");

    words.add(word);
    return word;
  }

  // Sentences API

  static Sentence getSentenceAt(int index) {
    return sentences[index % sentences.length];
  }

  static Sentence getNextSentenceContaining(
      {Word word, WordType wordType, int startingIndex = 0}) {
    for (int i = startingIndex; i < sentences.length; i++) {
      Sentence sentence = getSentenceAt(i);
      if (sentence.contains(word)) {
        return sentences[i];
      }

      List<Word> sentenceWords = MemoryRepository.getWords(sentence.wordIds);
      if (sentenceWords.any((element) => element.type == wordType)) {
        return sentences[i];
      }
    }
    for (int i = 0; i < startingIndex; i++) {
      Sentence sentence = getSentenceAt(i);
      if (sentence.contains(word)) {
        return sentences[i];
      }

      List<Word> sentenceWords = MemoryRepository.getWords(sentence.wordIds);
      if (sentenceWords.any((element) => element.type == wordType)) {
        return sentences[i];
      }
    }
    return null;
  }

  // Utility Methods

  static T returnNull<T>() {
    return null;
  }

  static String _generateId() {
    return uuid.v1();
  }

  static void update(Word word) {
    words.removeWhere((element) => element.id == word.id);
    words.add(word);
  }

  static Sentence addSentence(
      String english, List<SentenceValue> sentenceValues,
      {bool isQuestion = false}) {
    print("MemoryRepository.addSentence()");
    Sentence sentence = Sentence(
        id: _generateId(),
        sentenceValues: sentenceValues,
        english: english,
        isQuestion: isQuestion);
    sentences.add(sentence);
    return sentence;
  }

  static void deleteWord(Word word) {
    print("MemoryRepository.deleteWord(${word.id}");
    words.removeWhere((element) => element.id == word.id);
  }

  static void deleteSentence(Sentence sentence) {
    print("MemoryRepository.deleteSentence(${sentence.id})");
    sentences.remove(sentence);
  }

  static List<SentenceValue> findSentenceWords(String wordText) {
    print("MemoryRepository.findSentenceWords($wordText)");

    List<SentenceValue> values = [];
    sentences.forEach((element) {
      element.sentenceValues.forEach((sentenceValue) {
        if (sentenceValue.text == wordText) {
          values.add(sentenceValue);
        }
      });
    });
    return values;
  }
}
