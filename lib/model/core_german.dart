import 'dart:math';

import 'package:coregerman/model/entities/sentence.dart';
import 'package:coregerman/model/entities/sentence_value.dart';
import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/model/enums/case.dart';
import 'package:coregerman/model/enums/difficulty.dart';
import 'package:coregerman/model/enums/gender.dart';
import 'package:coregerman/model/enums/language.dart';
import 'package:coregerman/model/enums/modal_verbs.dart';
import 'package:coregerman/model/enums/pronouns.dart';
import 'package:coregerman/model/enums/sub_word_type.dart';
import 'package:coregerman/model/enums/tense.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/model/repositories/firestore_repository.dart';
import 'package:coregerman/model/repositories/memory_repository.dart';
import 'package:flutter/foundation.dart';

CoreGerman coreGerman;

class CoreGerman extends ChangeNotifier {
  bool loadingFinished = false;
  Random random = Random();
  List<int> sentenceShuffle = List();
  int sentenceShuffleIndex = 0;
  Difficulty difficulty = Difficulty.Expert;
  bool administrator = false;
  SentenceValue selectedSentenceValue;
  SentenceValue hoverSentenceValue;
  Word selectedWord;
  Word hoverWord;
  WordType selectedWordType;
  WordType hoverWordType;
  ModalVerb modalVerb;
  SubWordType selectedSubWordType;
  List<Word> previouslySelectedWords;
  Sentence selectedSentence;
  List<Sentence> sentences;
  Language language = Language.German;
  Case selectedCase;

  CoreGerman() {
    print("CoreGerman.Constructor()");
    coreGerman = this;
    synchronizeLocalDatabase();
  }

  Future synchronizeLocalDatabase() async {
    print("CoreGerman.synchronizeLocalDatabase()");

    bool sentencesLoaded = false;
    bool wordsLoaded = false;

    Function onSubJobFinished = () {
      if (sentencesLoaded && wordsLoaded) {
        onLoadingFinished();
      }
    };

    FirestoreRepository.getAllSentences().then((sentence) {
      print("CoreGerman: ${sentence.length} sentences loaded from Firestore");
      MemoryRepository.sentences = sentence;
      sentencesLoaded = true;
      sentenceShuffleIndex = 0;
      sentenceShuffle = List();
      for (int i = 0; i < sentence.length; i++) {
        sentenceShuffle.add(i);
      }
      sentenceShuffle.shuffle(random);
      onSubJobFinished();
    });

    FirestoreRepository.getAllWords().then((value) {
      print("CoreGerman: ${value.length} words loaded from Firestore");
      MemoryRepository.words = value;
      wordsLoaded = true;
      onSubJobFinished();
    });
  }

  void onLoadingFinished() {
    print("CoreGerman.onLoadingFinished()");
    loadingFinished = true;
    if (MemoryRepository.sentences.length > 0) {
      sentences = MemoryRepository.getRandomAnswers(max: 20);
    }
    refresh();
  }

  void shuffleSentences(){
    sentences = MemoryRepository.getRandomAnswers(max: 20);
    refresh();
  }

  WordType getSelectedWordType() {
    return selectedWordType ?? selectedWord?.type;
  }

  bool selectedWordTypeIs(WordType wordType) {
    return getSelectedWordType() == wordType;
  }

  void setDifficulty(Difficulty beginner) {}

  Word addWord(
    String value, {
    WordType wordType,
    Gender gender,
    Case kase,
    String english,
    ModalVerb modalVerb,
    Pronoun pronoun,
    Tense tense,
    Difficulty difficulty,
    SubWordType subWordType,
  }) {
    Word word = MemoryRepository.addWord(value,
        wordType: wordType,
        gender: gender,
        kase: kase,
        english: english,
        modalVerb: modalVerb,
        pronoun: pronoun,
        tense: tense,
        difficulty: difficulty,
        subWordType: subWordType);

    FirestoreRepository.saveWord(word);
    refresh();
    return word;
  }

  void deleteWord(Word word) {
    print("coreGerman.deleteWord(${word.value})");
    MemoryRepository.deleteWord(word);
    FirestoreRepository.deleteWord(word);
    refresh();
  }

  void updateWord(Word word) {}

  Sentence addSentence({String german, String english}) {
    print("coreGerman.addSentence($german)");
    bool isQuestion = german.endsWith("?");
    List<SentenceValue> sentenceValues = convertTextToSentenceValues(german);
    Sentence sentence = MemoryRepository.addSentence(english, sentenceValues,
        isQuestion: isQuestion);
    FirestoreRepository.saveSentence(sentence);
    return sentence;
  }

  void showAnswer(Sentence sentence) {}

  void selectWord(Word word) {
    if(selectedWord == word) return;

    print("CoreGermanService.selectWord(${word.value})");

    if (selectedSentenceValue != null && selectedSentenceValue.word == null) {
      setSentenceValueWord(selectedSentenceValue, word);
      return;
    }

    selectedWord = word;
    selectedSubWordType = word.subWordType;

    print("selectedSubWordType: $selectedSubWordType");

    if(!administrator){
      sentences = MemoryRepository.findSentencesContaining(word: word);
    }
    refresh();
  }

  void selectSentenceValue(SentenceValue sentenceValue) {

    if(selectedSentenceValue == sentenceValue) return;

    print("CoreGermanService.selectSentenceValue(${sentenceValue.text})");
    selectedSentenceValue = sentenceValue;
    selectedWord = selectedSentenceValue.word;
    selectedWordType = selectedWord?.type;
    selectedSubWordType = selectedWord?.subWordType;
    refresh();
  }

  void selectWordType(WordType wordType) {
    if (selectedWordType == wordType) return;

    print("coreGerman.selectWordType($wordType)");
    selectedWordType = wordType;
    selectedWord = null;
    selectedSentenceValue = null;
    selectedSubWordType = null;
    if (!administrator) {
      sentences = MemoryRepository.findSentencesContaining(wordType: wordType);
    }
    refresh();
  }

  void deselectAll({bool callRefresh = false}) {
    selectedWordType = null;
    selectedWord = null;
    selectedSubWordType = null;
    selectedSentenceValue = null;
    hoverSentenceValue = null;
    hoverWordType = null;
    hoverWord = null;
    if (callRefresh) {
      refresh();
    }
  }

  void setHoverWordType(WordType wordType) {
    if (hoverWordType == wordType) {
      return;
    }
    hoverWordType = wordType;
    refresh();
  }

  void setHoverSentenceValue(SentenceValue sentenceValue) {
    if (hoverSentenceValue == sentenceValue) {
      return;
    }
    hoverSentenceValue = sentenceValue;
    refresh();
  }

  void toggleAdministrationMode() {
    administrator = !administrator;
    refresh();
  }

  void setSentenceValueWord(SentenceValue sentenceValue, Word word) {
    print(
        "coreGerman.setSentenceValueWord(${sentenceValue.text}, ${word.value})");

    if (word == null) {
      throw Exception("word is null");
    }
    if (sentenceValue.sentence == null) {
      sentenceValue.sentence =
          MemoryRepository.fetchParentSentence(sentenceValue);
    }

    sentenceValue.wordId = word.id;
    saveSentence(sentenceValue.sentence);
    refresh();
  }

  void saveSelectedSentence(){
    print("coreGerman.saveSelectedSentence()");
    saveSentence(coreGerman.selectedSentence);
  }

  void saveSentence(Sentence sentence) {
    FirestoreRepository.saveSentence(sentence);
  }

  void selectCase(Case kase) {
    if (selectedCase == kase) return;

    print("coreGerman.selectCase($kase)");
    selectedCase = kase;
    sentences = MemoryRepository.findSentencesContaining(kase: kase);
    refresh();
  }

  void selectSentence(Sentence sentence) {
    print("coreGerman.selectSentence(${sentence.id})");
    sentences = [sentence];
    refresh();
  }

  void disconnectWord(SentenceValue selectedSentenceValue) {
    print("coreGerman.disconnectWord(${selectedSentenceValue.text})");
    selectedSentenceValue.wordId = null;
    selectedSentenceValue.save();
    refresh();
  }

  void deleteSentence(Sentence sentence) {
    print("coreGerman.deleteSentence(${sentence.id})");
    MemoryRepository.deleteSentence(sentence);
    FirestoreRepository.deleteSentence(sentence);
    sentences.remove(sentence);
    refresh();
  }

  List<SentenceValue> convertTextToSentenceValues(String german) {
    print("coreGerman.convertTextToSentenceValues($german)");
    german = german.replaceAll("?", "");
    german = german.replaceAll(",", "");
    german = german.replaceAll(".", "");
    german = german.replaceAll("-", "");
    return german.split(" ").map(mapTextToSentenceValue).toList();
  }

  SentenceValue mapTextToSentenceValue(String wordText) {
    List<Word> words = MemoryRepository.findWords(value: wordText);
    if (words.length == 1) {
      return SentenceValue(text: wordText, wordId: words[0].id);
    }
    List<SentenceValue> sentenceValues =
        MemoryRepository.findSentenceWords(wordText);
    if (sentenceValues.isNotEmpty &&
        sentenceValues.any((element) => element.wordId != sentenceValues[0])) {
      return SentenceValue(text: wordText, wordId: sentenceValues[0].wordId);
    }
    return SentenceValue(text: wordText);
  }

  void showExamplesFor(Word word) {
    setSentences(MemoryRepository.findSentencesContaining(word: word));
  }

  void setSentences(List<Sentence> value){
    print("coreGerman.setSentences()");
    sentences = value;
    refresh();
  }

  void refresh() {
    print("CoreGermanService.refresh()");
    notifyListeners();
  }
}
