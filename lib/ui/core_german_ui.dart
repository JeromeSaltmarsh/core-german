import 'package:coregerman/model/entities/sentence.dart';
import 'package:coregerman/model/core_german.dart';
import 'package:coregerman/model/entities/sentence_value.dart';
import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/model/enums/case.dart';
import 'package:coregerman/model/enums/difficulty.dart';
import 'package:coregerman/model/enums/gender.dart';
import 'package:coregerman/model/enums/modal_verbs.dart';
import 'package:coregerman/model/enums/pronouns.dart';
import 'package:coregerman/model/enums/sub_word_type.dart';
import 'package:coregerman/model/enums/tense.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/model/repositories/memory_repository.dart';
import 'package:coregerman/ui/configuration.dart';
import 'package:coregerman/ui/dialogs/add_word_dialog.dart';
import 'package:coregerman/ui/dialogs/edit_word_dialog.dart';
import 'package:coregerman/ui/dialogs/open_dialog.dart';
import 'package:coregerman/ui/dialogs/search_sentences_dialog.dart';
import 'package:coregerman/ui/global_utilities.dart';
import 'package:coregerman/ui/tables/adjectives_ui.dart';
import 'package:coregerman/ui/tables/adverbs_ui.dart';
import 'package:coregerman/ui/tables/articles.dart';
import 'package:coregerman/ui/tables/conjunctions_ui.dart';
import 'package:coregerman/ui/tables/keywords_ui.dart';
import 'package:coregerman/ui/tables/modal_verbs_ui.dart';
import 'package:coregerman/ui/tables/nouns.dart';
import 'package:coregerman/ui/tables/other_ui.dart';
import 'package:coregerman/ui/tables/prepositions_ui.dart';
import 'package:coregerman/ui/tables/pronouns_ui.dart';
import 'package:coregerman/ui/tables/questions_ui.dart';
import 'package:coregerman/ui/tables/verbs_ui.dart';
import 'package:coregerman/ui/ui_extensions.dart';
import 'package:coregerman/ui/widgets/simple_drop_down.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widgets/flutter_widgets.dart';
import 'package:provider/provider.dart';

class CoreGermanUI extends StatefulWidget {

  static BuildContext scaffoldContext;

  @override
  _CoreGermanUIState createState() => _CoreGermanUIState();

  static Future<Word> addWord({
    String initialValue = "",
    WordType wordType = WordType.Definite_Article,
    Gender gender = Gender.Masculine,
    Case kase = Case.Nominative,
    ModalVerb modalVerb,
    Pronoun pronoun,
    Tense tense,
    SubWordType subWordType,
  }) async {

    await scaffoldContext.buildDialog(
        AddWordDialog(
          initialValue: initialValue,
          wordType: wordType,
          gender: gender,
          kase: kase,
          modalVerb: modalVerb,
          pronoun: pronoun,
          tense: tense,
          subWordType: subWordType,
        ));
    coreGerman.refresh();
  }

  static Future editWord(Word word) {
    return scaffoldContext.buildDialog(EditWordDialog(word));
  }
}

class _CoreGermanUIState extends State<CoreGermanUI> {
  bool _initialized = false;

  @override
  Widget build(BuildContext context) {
    if (!_initialized) {
      print("CoreGermanUI.initializing()");
      _initialized = true;
      Provider.of<CoreGerman>(context, listen: true);
      return Center(child: CircularProgressIndicator());
    }

    print("CoreGermanUI.build()");

    return Scaffold(
        appBar: buildAppBar(context),
        body: buildBody(context),
        backgroundColor: UIConfiguration.backgroundColor,
        floatingActionButton: buildFloatActionButton(context));
  }

  Widget buildBody(BuildContext context) {
    CoreGermanUI.scaffoldContext = context;

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[buildSentences(), buildGrammar(context)],
      ),
    );
  }

  Widget buildAppBar(BuildContext context) {
    return AppBar(
        bottomOpacity: 0.0,
        elevation: 0.0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            GestureDetector(
                onTap: coreGerman.toggleAdministrationMode,
                child: Text(
                  "German by Doing",
                  style: TextStyle(
                      color: coreGerman.administrator
                          ? Colors.orange
                          : Colors.white),
                )),

            if (coreGerman.selectedWord == null)
              IconButton(
                icon: Icon(
                  Icons.refresh,
                  color: Colors.white,
                ),
                onPressed: coreGerman.shuffleSentences,
              ),
            if (coreGerman.selectedWord != null)
              Row(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      coreGerman.deselectAll(callRefresh: true);
                    },
                    child: Container(
                        margin: EdgeInsets.symmetric(horizontal: 10),
                        child: const Icon(
                          Icons.arrow_back,
                          color: Colors.white,
                        )),
                  ),
                  Text(coreGerman.selectedWord.value),
                  const SizedBox(
                    width: 30,
                  ),
                  IconButton(
                    tooltip:
                        "Find examples containing ${coreGerman.selectedWord.value}",
                    icon: Icon(Icons.list),
                    onPressed: () {
                      coreGerman.showExamplesFor(coreGerman.selectedWord);
                    },
                  )
//                if(coreGermanService.previouslySelectedWords != null && coreGermanService.previouslySelectedWords.length > 1)
//                  Text(coreGermanService.previouslySelectedWords[coreGermanService.previouslySelectedWords.length - 1].value),
                ],
              ),
            const SizedBox(
              width: 150,
            ),
            if (coreGerman.administrator)
              buildAppBarAdministrationTools(context),
//            if (CoreGermanUI.displayDifficultySettings &&
//                coreGerman != null &&
//                state is DisplayingQuestionAnswer)
//              buildDifficulty(context),
          ],
        ));
  }

  Widget buildAppBarAdministrationTools(BuildContext context) {
    return Row(
      children: <Widget>[
        FlatButton(
          child: Text("Add Word"),
          onPressed: () {
            showAddWordDialog(context, wordType: coreGerman.selectedWordType);
          },
        ),
        FlatButton(
            child: Text("Add Sentence"),
            onPressed: () {
              showAddSentenceDialog(context);
            }),
        if (coreGerman.selectedSentenceValue != null)
          FlatButton(
            child: Text("Edit Value"),
            onPressed: () async {
              await showEditSentenceValue(
                  context, coreGerman.selectedSentenceValue);
              coreGerman.saveSelectedSentence();
            },
          ),
        if (coreGerman.selectedSentenceValue != null &&
            coreGerman.selectedSentenceValue.wordId != null)
          FlatButton(
            child: Text("Disconnect"),
            onPressed: () async {
              coreGerman.disconnectWord(coreGerman.selectedSentenceValue);
            },
          ),
        if (coreGerman.selectedWord != null)
          FlatButton(
            child: Text("Edit Word"),
            onPressed: () {
              showEditWordDialog(context, coreGerman.selectedWord);
            },
          ),
      ],
    );
  }

  Widget buildSearch(BuildContext context) {
    return Container(
//           width: double.infinity,
        height: 25,
        width: 300,
        alignment: Alignment.center,
        child: TextField(
          decoration: InputDecoration(
              fillColor: Colors.white,
              icon: Icon(
                Icons.search,
                color: Colors.white54,
              )),
        ));
  }

  Future<Word> showAddWordDialog(
    BuildContext context, {
    String initialValue = "",
    WordType wordType = WordType.Definite_Article,
    Gender gender = Gender.Masculine,
    Case kase = Case.Nominative,
    ModalVerb modalVerb,
    Pronoun pronoun,
    Tense tense,
    SubWordType subWordType,
  }) {
    return buildDialog(
        context,
        AddWordDialog(
          initialValue: initialValue,
          wordType: wordType,
          gender: gender,
          kase: kase,
          modalVerb: modalVerb,
          pronoun: pronoun,
          tense: tense,
          subWordType: subWordType,
        ));
  }

  Future showEditWordDialog(BuildContext context, Word word) {
    return _buildDialog(context, EditWordDialog(word));
  }

  Future showEditSentenceValue(
      BuildContext context, SentenceValue sentenceValue) async {
    return showDialog(
        context: context,
        builder: (dialogContext) {
          return AlertDialog(
            content: TextField(
              controller: TextEditingController(text: sentenceValue.text),
              onChanged: (value) {
                sentenceValue.text = value;
              },
              autofocus: true,
            ),
            actions: [
              FlatButton(
                child: Text("Cancel"),
                onPressed: () {
                  Navigator.pop(dialogContext);
                },
              ),
              FlatButton(
                child: Text("Submit"),
                onPressed: () {
                  sentenceValue.save();
                  coreGerman.refresh();
                  Navigator.pop(dialogContext);
                },
              ),
            ],
          );
        });
  }

  Future showAddSentenceDialog(BuildContext context) {
    print("coreGermanUI.showAddSentenceDialog()");

    String german = "";
    String english = "";

    return buildDialog(
        context,
        AlertDialog(
          title: Text("Add Sentence"),
          content: Container(
            width: 500,
            height: 200,
            child: Column(
              children: [
                TextField(
                  decoration: InputDecoration(labelText: "German Translation"),
                  autofocus: true,
                  onChanged: (value) {
                    german = value;
                  },
                ),
                TextField(
                  decoration: InputDecoration(labelText: "English Translation"),
                  autofocus: true,
                  onChanged: (value) {
                    english = value;
                  },
                ),
              ],
            ),
          ),
          actions: [
            FlatButton(
              child: Text("Cancel"),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            FlatButton(
              child: Text("Submit"),
              onPressed: () {
                if (german.isNotEmpty || english.isNotEmpty) {
                  Sentence sentence =
                      coreGerman.addSentence(german: german, english: english);
                  coreGerman.selectSentence(sentence);
                  Navigator.pop(context);
                }
              },
            ),
          ],
        ));
  }

  Future showEditSentenceDialog(BuildContext context, Sentence sentence) {
    String german = sentence.text;

    return showDialog(
        context: context,
        builder: (dialogContext) {
          return AlertDialog(
            title: Text("Edit Sentence"),
            content: Container(
              width: MediaQuery.of(context).size.width * GoldenRatio,
              height: MediaQuery.of(context).size.height * GoldenRatioA,
              child: Column(
                children: [
                  TextField(
                    decoration: InputDecoration(labelText: "German"),
                    controller: TextEditingController(text: sentence.text),
                    autofocus: true,
                    onChanged: (value) {
                      german = value;
                    },
                  ),
                  TextField(
                    decoration: InputDecoration(labelText: "English"),
                    controller: TextEditingController(text: sentence.english),
                    autofocus: false,
                    onChanged: (value) {
                      sentence.english = value;
                    },
                  ),
                ],
              ),
            ),
            actions: [
              FlatButton(
                child: Text("Cancel"),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                child: Text("Delete"),
                onPressed: () {
                  coreGerman.deleteSentence(sentence);
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                child: Text("Submit"),
                onPressed: () {
                  sentence.sentenceValues =
                      coreGerman.convertTextToSentenceValues(german);
                  sentence.save();
                  coreGerman.refresh();
                  Navigator.pop(dialogContext);
                },
              ),
            ],
          );
        });
  }

  Future<T> _buildDialog<T>(BuildContext context, Widget content) {
    return showDialog<T>(
        context: context,
        builder: (dialogContext) {
          return content;
        });
  }

  Widget buildDifficulty(BuildContext context) {
    double starSize = 22;

    return Row(
      children: <Widget>[
        Expanded(
          child: SizedBox(),
        ),
        Container(
            constraints: BoxConstraints(minWidth: 70),
            child: Text(
              getText(
                coreGerman.difficulty,
              ),
              style: TextStyle(fontSize: 15),
            )),
        SizedBox(
          width: 15,
        ),
        Tooltip(
          message: getText(Difficulty.Beginner),
          child: GestureDetector(
            onTap: () {
              coreGerman.setDifficulty(Difficulty.Beginner);
            },
            child: Icon(Icons.star, size: starSize),
          ),
        ),
        Tooltip(
          message: getText(Difficulty.Intermediate),
          child: GestureDetector(
              onTap: () {
                coreGerman.setDifficulty(Difficulty.Intermediate);
              },
              child: Icon(
                coreGerman.difficulty == Difficulty.Beginner
                    ? Icons.star_border
                    : Icons.star,
                size: starSize,
              )),
        ),
        Tooltip(
          message: getText(Difficulty.Expert),
          child: GestureDetector(
              onTap: () {
                coreGerman.setDifficulty(Difficulty.Expert);
              },
              child: Icon(
                  coreGerman.difficulty != Difficulty.Expert
                      ? Icons.star_border
                      : Icons.star,
                  size: starSize)),
        ),
        SizedBox(
          width: 15,
        ),
      ],
    );
  }

  Widget buildFloatActionButton(BuildContext context) {
    return FloatingActionButton.extended(
      label: Text(
        "Search",
        style: TextStyle(color: Colors.white54),
      ),
      onPressed: () {
        buildDialog(context, SearchSentencesDialog());
      },
      icon: Icon(Icons.search),
    );
  }

  Widget buildGrammar(BuildContext context) {
    return Column(
      children: <Widget>[
        buildWordTypeBar(context),
        const SizedBox(
          height: 10,
        ),
        buildWordTypePanel(context),
        if (coreGerman.selectedSentenceValue != null &&
            coreGerman.selectedSentenceValue.wordId == null)
          Container(
//              width: 60,
              height: 60,
              alignment: Alignment.center,
              child: Text(
                "${coreGerman.selectedSentenceValue.text} - Missing Connection",
                style: TextStyle(
                    color: UIConfiguration.defaultColor,
                    fontSize: UIConfiguration.fontSize),
              ))
      ],
    );
  }

  Widget buildWordTypeBar(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double fullScreenWidth = 700;
    bool fullScreen = screenWidth >= fullScreenWidth;

    if (fullScreen) {
      return Container(
        width: double.infinity,
        color: UIConfiguration.lightLightGrey,
        padding: EdgeInsets.symmetric(horizontal: 100),
        child: Row(
            children: WordType.values
                .map((wordType) => buildWordTypeWidget(context, wordType))
                .toList()),
      );
    }

    return SimpleDropDown<WordType>(
      WordType.values,
      onChanged: (value) {
        coreGerman.selectWordType(value);
      },
      initialValue: coreGerman.selectedWordType,
    );
  }

  Widget buildWordTypeWidget(BuildContext context, WordType wordType) {
    bool selected = coreGerman.selectedWordType == (wordType);
    bool isHover = !selected &&
        (coreGerman.hoverWord?.type == wordType ||
            coreGerman.hoverWordType == wordType);

    if (coreGerman.hoverSentenceValue != null &&
        coreGerman.hoverSentenceValue.wordId != null) {
      Word word =
          MemoryRepository.findWordById(coreGerman.hoverSentenceValue.wordId);
      if (word.type == wordType) {
        isHover = true;
      }
    }

    return DragTarget<SentenceValue>(onAccept: (sentenceValue) async {
      if (sentenceValue.wordId == null) {
        Word newWord = await showAddWordDialog(context,
            initialValue: sentenceValue.text, wordType: wordType);
        if (newWord != null) {
          coreGerman.setSentenceValueWord(sentenceValue, newWord);
        }
      }
    }, builder: (context, candidateData, rejectedData) {
      return MouseRegion(
        onHover: (event) {
          coreGerman.setHoverWordType(wordType);
        },
        onExit: (event) {
          coreGerman.setHoverWordType(null);
        },
        child: GestureDetector(
          onTap: () {
            coreGerman.selectWordType(wordType);
          },
          child: Container(
//              width: 110,
              margin: EdgeInsets.symmetric(horizontal: 10),
              alignment: Alignment.center,
              color: selected
                  ? UIConfiguration.lightGrey
                  : UIConfiguration.lightLightGrey,
              padding: EdgeInsets.symmetric(vertical: 20),
              child: Text(
                wordType.text,
                textAlign: TextAlign.center,
                style: TextStyle(
                  decoration: isHover ? TextDecoration.underline : null,
                  color: selected
                      ? Colors.black
                      : isHover
                          ? UIConfiguration.wordTypeHoverColor
                          : UIConfiguration.wordTypeColor,
                ),
              )),
        ),
      );
    });
  }

  Widget buildWordTypePanel(BuildContext buildContext) {
    switch (coreGerman.selectedWordType) {
      case WordType.Keyword:
        return KeywordsUI();
      case WordType.Definite_Article:
        return Articles(WordType.Definite_Article);
      case WordType.Indefinite_Article:
        return Articles(WordType.Indefinite_Article);
      case WordType.Noun:
        return NounsUI();
      case WordType.Verb:
        return VerbsUI();
      case WordType.Modal_Verb:
        return ModalVerbsUI();
      case WordType.Pronoun:
        return PronounsUI();
      case WordType.Question:
        return QuestionsUI();
      case WordType.Adjective:
        return AdjectivesUI();
      case WordType.Preposition:
        return PrepositionsUI();
      case WordType.Adverb:
        return AdverbsUI();
      case WordType.Conjunction:
        return ConjuctionsUI();
//      case WordType.Overview:
//        return OverViewUI();
      case WordType.Unmapped:
        return OtherUI();
      default:
        return SizedBox();
    }
  }

  Widget buildSentences() {
    List<Sentence> sentences = coreGerman.sentences;

    if (sentences == null || sentences.isEmpty) {
      return Container(
          height: UIConfiguration.sentencePanelHeight,
          alignment: Alignment.center,
          child: Text("No examples found"));
    }

    int initialScrollIndex = sentences.indexOf(coreGerman.selectedSentence);

    if (initialScrollIndex == -1) {
      initialScrollIndex = 0;
    }

    return Container(
      height: UIConfiguration.sentencePanelHeight,
      child: ScrollablePositionedList.builder(
          initialScrollIndex: initialScrollIndex,
          itemCount: sentences.length,
          itemBuilder: (builderContext, index) {
            return buildSentence(sentences[index]);
          }),
    );
  }

  Widget buildSentence(Sentence sentence) {
    double screenWidth = MediaQuery.of(context).size.width;
    double marginLeft = screenWidth > 800 ? screenWidth * GoldenRatioA : 10;

    return Container(
      padding: EdgeInsets.symmetric(
          vertical: UIConfiguration.sentenceVerticalPadding),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: marginLeft,
          ),
          if (coreGerman.administrator)
            IconButton(
              icon: Icon(
                Icons.edit,
                color: sentence.english == null || sentence.english.isEmpty
                    ? Colors.orange
                    : Colors.black,
              ),
              onPressed: () {
                showEditSentenceDialog(context, sentence);
              },
            ),
          if (!coreGerman.administrator)
            Tooltip(
              message: sentence.english ?? "Translation Missing",
              child: IconButton(
                icon: Icon(Icons.star_border),
                onPressed: () {},
              ),
            ),
          ...sentence.sentenceValues.map(
            (sentenceValue) {
              return buildSentenceValue(sentenceValue, sentence);
            },
          ).toList(),
          if (sentence.isQuestion)
            Text(
              "?",
              style: TextStyle(color: UIConfiguration.defaultColor),
            )
        ],
      ),
    );
  }

  Widget buildSentenceValue(SentenceValue sentenceValue, Sentence sentence) {
    Word word = MemoryRepository.findWordById(sentenceValue.wordId);
    bool wordSelected = coreGerman.selectedSentenceValue == sentenceValue ||
        coreGerman.selectedWord != null && coreGerman.selectedWord == word;

    bool wordTypeHovered = coreGerman.hoverWordType != null &&
        coreGerman.hoverWordType == word?.type;
    bool wordTypeSelected = coreGerman.selectedWordType != null &&
        coreGerman.selectedWordType == word?.type;

    bool underline = wordTypeHovered || wordTypeSelected;
    bool outline = wordSelected;

    if (coreGerman.selectedWord != null && coreGerman.selectedWord != word) {
      underline = false;
    }

    if (outline) {
      underline = false;
    }

    Color color = word != null ? word.color : UIConfiguration.defaultColor;

    if (word == null && coreGerman.administrator) {
      color = Colors.orange;
    }

    return Draggable<SentenceValue>(
      data: sentenceValue,
      feedback: Text(
        sentenceValue.text,
        style: TextStyle(color: color),
      ),
      child: MouseRegion(
          onHover: (value) {
            coreGerman.setHoverSentenceValue(sentenceValue);
          },
          onExit: (value) {
            coreGerman.setHoverSentenceValue(null);
          },
          child: Tooltip(
            message: word == null || word.english == null ? "?" : word.english,
            child: GestureDetector(
              onTap: () {
                coreGerman.selectSentenceValue(sentenceValue);
              },
              child: Container(
                  padding: EdgeInsets.all(2),
                  decoration: outline
                      ? BoxDecoration(
                          border: Border.all(
                              color: color,
                              width: 1.0,
                              style: BorderStyle.solid),
                          borderRadius: SelectedBorderRadius)
                      : null,
                  child: Text(
                    sentenceValue.text,
                    style: TextStyle(
                        color: color,
                        decoration:
                            underline ? TextDecoration.underline : null),
                  )),
            ),
          )),
    );
  }
}
