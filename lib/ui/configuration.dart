import 'package:flutter/material.dart';

class UIConfiguration {

  static double sentencePanelHeight = 250;
  static double sentenceVerticalPadding = 10;

  static Color feminineColor = Colors.red;
  static Color masculineColor = Colors.black;
  static Color neutralColor = Colors.green;
  static Color pluralColor = Colors.purple;
  static Color defaultColor = Colors.blue;
  static Color underlineColor = Colors.blueAccent;
  static Color defaultNotSelected = Color.fromARGB(250, 100, 100, 100);
  static Color lightLightGrey = Color.fromARGB(250, 240, 240, 240);
  static Color lightGrey = Color.fromARGB(250, 250, 250, 250);
  static Color backgroundColor = lightGrey;
  static Color nextSentenceButtonColor = Colors.blue;
  static Color nextSentenceButtonColorSplashColor = Colors.green;
  static Color wordTypeColor = Color.fromARGB(250, 150, 150, 150);
  static Color wordTypeHoverColor = Color.fromARGB(250, 80, 80, 80);
  static Color unselectedTextColor =  Color.fromARGB(250, 150, 150, 150);
  static Color selectedTextColor = Color.fromARGB(250, 80, 80, 80);
  static double wordPadding = 2.5;
  static double fontSize = 17;
  static EdgeInsets wordMargin = EdgeInsets.all(wordPadding);

  static ThemeData theme = ThemeData(primarySwatch: Colors.blue);
  static Color lockColor = Color.fromARGB(250, 200, 200, 200);
  static Color tableColor = Color.fromARGB(250, 200, 200, 200);
}
