
import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/ui/widgets/word_ui.dart';
import 'package:flutter/material.dart';

import '../configuration.dart';

class WordColumn extends StatelessWidget{

  final List<Word> words;
  final bool isSelected;
  final String title;
  final double width;

  final double Height = 220;

  WordColumn(this.title, {this.words, this.isSelected = false, this.width = 100});

  @override
  Widget build(BuildContext context) {
      return Container(
        child: Column(
            children: [
              if(title != null)
              Container(
                  width: 100,
                  child: Text(title, style: TextStyle(
                      fontWeight: isSelected ? FontWeight.bold : FontWeight.normal,
                      color: isSelected ? Colors.black : UIConfiguration.defaultNotSelected
                  ),)),
              SizedBox(height: 5,),
              Container(
                height: Height,
                width: 100,
                child: ListView.builder(
                    itemCount: words.length,
                    itemBuilder: (builderContext, index) {
                      return WordUI(words[index]);
                    }),
              )
            ]
        ),
      );
    }

}