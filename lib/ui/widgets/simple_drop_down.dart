import 'package:flutter/material.dart';

class SimpleDropDownController<T>{
  T value;

  SimpleDropDownController([this.value]);


}

class SimpleDropDown<T> extends StatefulWidget {
  final T initialValue;
  final List<T> values;
  final ValueChanged<T> onChanged;
  final SimpleDropDownController controller;

  SimpleDropDown(this.values, {this.onChanged, this.initialValue, this.controller});

  @override
  _SimpleDropDownState<T> createState() => _SimpleDropDownState<T>();
}


class _SimpleDropDownState<T> extends State<SimpleDropDown<T>> {
  T selectedValue;

  @override
  Widget build(BuildContext context) {
    return DropdownButton<T>(
      value: selectedValue ?? widget.initialValue ?? widget.values[0],
      items: widget.values.map((value) {
        return DropdownMenuItem<T>(
          value: value,
          child: Text(value.toString()),
        );
      }).toList(),
      onChanged: (value) {
        setState(() {
          selectedValue = value;
          if(widget.controller != null){
            widget.controller.value = selectedValue;
          }

          if (widget.onChanged != null) {
            widget.onChanged(selectedValue);
          }
        });
      },
    );
  }
}
