
import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/ui/configuration.dart';
import 'package:flutter/material.dart';

class LockedWord extends StatelessWidget{

  final Word word;

  LockedWord(this.word);

  @override
  Widget build(BuildContext context) {
    return Tooltip(
        message: "${word.value} (${word.english})",
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Icon(
            Icons.lock,
            color: UIConfiguration.lockColor,
            size: 15,
          ),
        ));
  }

}