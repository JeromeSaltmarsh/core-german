
import 'package:flutter/material.dart';

class CircleButton extends StatelessWidget{

  final Function onTap;
  final double width;
  final double height;
  final Widget child;
  final Color color;
  final Color splashColor;

  CircleButton({this.onTap, this.width = 40, this.height = 40, this.child, this.color, this.splashColor});

  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: Material(
        color: color, // button color
        child: InkWell(
          splashColor: splashColor, // inkwell color
          child: SizedBox(width: width, height: height, child: child),
          onTap: onTap,
        ),
      ),
    );
  }

}