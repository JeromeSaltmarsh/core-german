
import 'package:coregerman/ui/widgets/cell.dart';
import 'package:flutter/material.dart';

class GenderRow extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Cell(Text(" ")),
        Cell(Tooltip(
          message: "Masculine",
          child: Text("Masculine"),)),
        Cell(Tooltip(
            message: "Feminine",
            child: Text("Feminine"))),
        Cell(Tooltip(
            message: "Neutral",
            child: Text("Neutral"))),
        Cell(Tooltip(
            message: "Plural",
            child: Text("Plural"))),
      ],
    );
  }

}