
import 'package:flutter/material.dart';

class Cell extends StatelessWidget{

  final Widget child;
  final double width;
  final double height;
  final Alignment alignment;

  Cell(this.child, {this.width = 80, this.height = 40, this.alignment = Alignment.center});

  @override
  Widget build(BuildContext context) {
    return Container(width: width, height: height, child: child, alignment: alignment,);
  }

}