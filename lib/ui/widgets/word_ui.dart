import 'package:coregerman/model/core_german.dart';
import 'package:coregerman/model/entities/sentence_value.dart';
import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/model/enums/case.dart';
import 'package:coregerman/model/enums/gender.dart';
import 'package:coregerman/model/enums/modal_verbs.dart';
import 'package:coregerman/model/enums/pronouns.dart';
import 'package:coregerman/model/enums/sub_word_type.dart';
import 'package:coregerman/model/enums/tense.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/model/repositories/memory_repository.dart';
import 'package:coregerman/ui/configuration.dart';
import 'package:coregerman/ui/core_german_ui.dart';
import 'package:coregerman/ui/ui_extensions.dart';
import 'package:flutter/material.dart';

class WordUI extends StatelessWidget {
  final Word word;

  static const SelectedBorderRadius =
      const BorderRadius.all(const Radius.circular(4));

  WordUI(this.word);

  @override
  Widget build(BuildContext context) {
//    if (!coreGerman.isWithinDifficulty(word: word)) {
//      return LockedWord(word);
//    }

    String text = word.value;
    bool isSelected = coreGerman.selectedWord == word;

    bool underline = false;

    if (!isSelected &&
        coreGerman.hoverSentenceValue != null &&
        coreGerman.hoverSentenceValue.wordId == word.id) {
      underline = true;
    }

    return DragTarget<SentenceValue>(
      builder: (context, candidateData, rejectedData) {
        return GestureDetector(
          onTap: onTapped,
          child: Tooltip(
            message: word.english ?? "?",
            child: Container(
                decoration: isSelected
                    ? BoxDecoration(
                        border: Border.all(
                            color: word.color,
                            width: 1.0,
                            style: BorderStyle.solid),
                        borderRadius: SelectedBorderRadius)
                    : null,
                padding: UIConfiguration.wordMargin,
                child: Text(text,
                    style: TextStyle(
                      decoration: underline ? TextDecoration.underline : null,
                      fontSize: UIConfiguration.fontSize,
                      color: word.color,
                    ))),
          ),
        );
      },
      onAccept: (sentenceValue) {
        print("wordUI.onAccept()");
        coreGerman.setSentenceValueWord(sentenceValue, word);
      },
    );
  }

  bool get selectedSentenceContainsWord {
    return coreGerman.selectedSentence != null &&
        coreGerman.selectedSentence.contains(word);
  }

  void onTapped() {
    print("WordUI.onTapped()");
    coreGerman.selectWord(word);
  }

  static Widget findBy(
      {WordType wordType,
      Gender gender,
      Case kase,
      String id,
      ModalVerb modalVerb,
      Pronoun pronoun,
      Tense tense,
      SubWordType subWordType}) {
    Word word = MemoryRepository.findWord(
      wordType: wordType,
      gender: gender,
      kase: kase,
      id: id,
      modalVerb: modalVerb,
      pronoun: pronoun,
      tense: tense,
      subWordType: subWordType,
    );

    if (word == null) {
      return GestureDetector(
        child: Text(coreGerman.administrator ? "?" : "-"),
        onTap: () async {

          await CoreGermanUI.addWord(
            wordType: wordType,
            gender: gender,
            kase: kase,
            modalVerb: modalVerb,
            pronoun: pronoun,
            tense: tense,
            subWordType: subWordType,
          );
        },
      );
    }

    return WordUI(
      word,
    );
  }
}
