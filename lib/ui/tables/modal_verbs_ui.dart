import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/model/enums/modal_verbs.dart';
import 'package:coregerman/model/enums/pronouns.dart';
import 'package:coregerman/model/enums/tense.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/model/repositories/memory_repository.dart';
import 'package:coregerman/ui/configuration.dart';
import 'package:coregerman/ui/global_utilities.dart';
import 'package:coregerman/ui/widgets/cell.dart';
import 'package:coregerman/ui/widgets/word_ui.dart';
import 'package:flutter/material.dart';
import 'package:coregerman/model/core_german.dart';
import 'package:coregerman/ui/ui_extensions.dart';

class ModalVerbsUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        buildVerbMenu(context),
        if (coreGerman.modalVerb != null ||
            coreGerman.selectedWordTypeIs(WordType.Modal_Verb))
          buildModalVerbs(context),
      ],
    );
  }

  buildVerbMenu(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: ModalVerb.values.map((modalVerb) {
        bool isSelected = coreGerman.modalVerb == modalVerb;

        Word word = MemoryRepository.findWord(
            wordType: WordType.Modal_Verb,
            subWordType: modalVerb.subWordType,
            condition: (word) => word.tense == null);

        if (word != null) {
          return Padding(
            padding: const EdgeInsets.all(10.0),
            child: WordUI(word),
          );
        }

        return FlatButton(
          color: isSelected ? UIConfiguration.lightLightGrey : null,
          child: Text(
            getText(modalVerb).toLowerCase(),
            style: TextStyle(
                color: isSelected
                    ? UIConfiguration.selectedTextColor
                    : UIConfiguration.unselectedTextColor),
          ),
          onPressed: () {
            context.showAddSubWordTypeWord(modalVerb.subWordType);
          },
        );
      }).toList(),
    );
  }

  Widget buildModalVerbs(BuildContext context) {
    if (coreGerman.selectedSubWordType == null) return SizedBox();

    List<Tense> tenses = [Tense.Past, Tense.Present];
    List<Pronoun> pronouns = [Pronoun.Ich, Pronoun.Du, Pronoun.Er_Sie_Es, Pronoun.Wir, Pronoun.Ihr, Pronoun.Sie_They];

    return Column(
      children: <Widget>[
        Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Cell(null),
          ...tenses
              .map((tense) => Tooltip(
                  message: tense.description,
                  child: Cell(Text(getText(tense).toLowerCase()))))
              .toList(),
        ]),
        ...pronouns
            .map((verbPronoun) {
          return Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Cell(
              Text(getText(verbPronoun).toLowerCase()),
              alignment: Alignment.centerLeft,
            ),
            ...tenses
                .map((tense) => Cell(WordUI.findBy(
                    wordType: WordType.Modal_Verb,
                    modalVerb: coreGerman.modalVerb,
                    subWordType: coreGerman.selectedSubWordType,
                    pronoun: verbPronoun,
                    tense: tense)))
                .toList(),
          ]);
        })
      ],
    );
  }
}
