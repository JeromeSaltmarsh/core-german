import 'package:coregerman/model/core_german.dart';
import 'package:coregerman/model/enums/case.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:flutter/cupertino.dart';

class OverViewUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(25),
      child: Container(
        width: 400,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            buildCases(),
            buildGenders(),
            buildTenses(),
          ],
        ),
      ),
    );
  }

  Widget buildCases() {
    return Container(
      child: Column(
        children: [
          Text(
            "Cases",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          GestureDetector(
            child: Text("Nominative"),
            onTap: () {
              coreGerman.selectCase(Case.Nominative);
            },
          ),
          GestureDetector(
            child: Text("Accusative"),
            onTap: () {
              coreGerman.selectCase(Case.Accusative);
            },
          ),
          GestureDetector(
            child: Text("Dative"),
            onTap: () {
              coreGerman.selectCase(Case.Dative);
            },
          ),
          GestureDetector(
            child: Text("Genitive"),
            onTap: () {
              coreGerman.selectCase(Case.Genitive);
            },
          ),
          Container(
            margin: EdgeInsets.all(15),
            child: Column(
              children: [
                GestureDetector(child: Text("Definite Articles"),
                onTap: (){
                },),
                Text("Indefinite Articles"),
                Text("Verbs"),
                Text("Prepositions"),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget buildGenders() {
    return Column(
      children: [
        Text(
          "Genders",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        Text("Masculine"),
        Text("Feminine"),
        Text("Neutral"),
        Text("Plural"),
        Container(
          margin: EdgeInsets.all(15),
          child: Column(
            children: [
              GestureDetector(child: Text("Nouns"),
                onTap: (){
                },),
            ],
          ),
        )
      ],
    );
  }

  Widget buildTenses() {
    return Column(
      children: [
        Text(
          "Tenses",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        Text("Past"),
        Text("Present"),
        Text("Past Perfect"),
        Text("etc..."),
        Container(
          margin: EdgeInsets.all(15),
          child: Column(
            children: [
              GestureDetector(child: Text("Verbs"),
                onTap: (){
                },),
            ],
          ),
        )
      ],
    );
  }
}
