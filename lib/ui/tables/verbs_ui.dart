
import 'package:coregerman/model/enums/case.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/model/repositories/memory_repository.dart';
import 'package:coregerman/ui/widgets/word_column.dart';
import 'package:flutter/material.dart';

class VerbsUI extends StatelessWidget{

  @override
  Widget build(BuildContext context) {

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        WordColumn("Nominative", words: MemoryRepository.findWords(wordType: WordType.Verb, kase: Case.Nominative), isSelected: false),
        WordColumn("Accusative", words: MemoryRepository.findWords(wordType: WordType.Verb, kase: Case.Accusative), isSelected: false),
        WordColumn("Dative", words: MemoryRepository.findWords(wordType: WordType.Verb, kase: Case.Dative), isSelected: false),
      ],
    );
  }

}