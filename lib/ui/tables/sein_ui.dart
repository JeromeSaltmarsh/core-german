import 'package:coregerman/model/enums/pronouns.dart';
import 'package:coregerman/model/enums/sub_word_type.dart';
import 'package:coregerman/model/enums/tense.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/ui/widgets/cell.dart';
import 'package:coregerman/ui/widgets/word_ui.dart';
import 'package:flutter/material.dart';

import 'package:coregerman/ui/global_utilities.dart';

class SeinUI extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Cell(null),
          Cell(Text("Past")),
          Cell(Text("Present"))
        ],
      ),
      ...Pronoun.values
          .where((element) => element.isThirdPersonSingular)
          .map((pronoun) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Cell(Text(getText(pronoun)), alignment: Alignment.centerLeft,),
            Cell(WordUI.findBy(
                wordType: WordType.Keyword,
                subWordType: SubWordType.Keyword_Sein,
                pronoun: pronoun,
                tense: Tense.Past)),
            Cell(WordUI.findBy(
                wordType: WordType.Keyword,
                subWordType: SubWordType.Keyword_Sein,
                pronoun: pronoun,
                tense: Tense.Present)),
          ],
        );
      }).toList()
    ]);
  }

  static Widget buildSubType(SubWordType subWordType){
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Cell(null),
              Cell(Text("Past")),
              Cell(Text("Present"))
            ],
          ),
          ...Pronoun.values
              .where((element) => element.isThirdPersonSingular)
              .map((pronoun) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Cell(Text(getText(pronoun)), alignment: Alignment.centerLeft,),
                Cell(WordUI.findBy(
                    wordType: WordType.Keyword,
                    subWordType: subWordType,
                    pronoun: pronoun,
                    tense: Tense.Past)),
                Cell(WordUI.findBy(
                    wordType: WordType.Keyword,
                    subWordType: subWordType,
                    pronoun: pronoun,
                    tense: Tense.Present)),
              ],
            );
          }).toList()
        ]);
  }
}
