
import 'package:coregerman/ui/global_utilities.dart';
import 'package:coregerman/model/enums/case.dart';
import 'package:coregerman/model/enums/gender.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/ui/widgets/cell.dart';
import 'package:coregerman/ui/widgets/gender_row.dart';
import 'package:coregerman/ui/widgets/word_ui.dart';
import 'package:flutter/material.dart';

class Articles extends StatelessWidget {

  final WordType wordType;

  Articles(this.wordType);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        GenderRow(),
        buildCase(Case.Nominative),
        buildCase(Case.Accusative),
        buildCase(Case.Dative),
        buildCase(Case.Genitive),
      ],
    );
  }

  Widget buildCase(Case kase) {
    return Container(
      child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
        Tooltip(
          message: CaseUtil.getDescription(kase),
          child: Cell(
            Text(
              getText(kase),
            ),
            alignment: Alignment.centerLeft,
          ),
        ),
        ...Gender.values.map((gender) {
          return Cell(
              WordUI.findBy(wordType: wordType, gender: gender, kase: kase));
        })
      ]),
    );
  }
}
