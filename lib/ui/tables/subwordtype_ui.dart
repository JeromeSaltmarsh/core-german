import 'package:coregerman/model/enums/pronouns.dart';
import 'package:coregerman/model/enums/sub_word_type.dart';
import 'package:coregerman/model/enums/tense.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/ui/global_utilities.dart';
import 'package:coregerman/ui/widgets/cell.dart';
import 'package:coregerman/ui/widgets/word_ui.dart';
import 'package:flutter/material.dart';
import 'package:coregerman/model/core_german.dart';

class SubWordTypeUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    switch (coreGerman.selectedSubWordType) {
      case SubWordType.Article_Definite:
        break;
      case SubWordType.Article_Indefinite:
        break;
      case SubWordType.ModalVerb_Duerfen:
        break;
      case SubWordType.ModalVerb_Koennen:
        break;
      case SubWordType.Pronoun_Possessive:
        break;
      case SubWordType.Pronoun_Personal:
        break;
      case SubWordType.Keyword_Sein:
        return buildSubType(SubWordType.Keyword_Sein);
      case SubWordType.Keyword_Haben:
        return buildSubType(SubWordType.Keyword_Haben);
      case SubWordType.Keyword_Werden:
        return buildSubType(SubWordType.Keyword_Werden);
      case SubWordType.Keyword_Machen:
        return buildSubType(SubWordType.Keyword_Machen);
      case SubWordType.Keyword_Gewesen:
        break;
      case SubWordType.Keyword_Waere:
        return buildSingleColumn(SubWordType.Keyword_Waere);
      case SubWordType.Keyword_Haette:
        return buildSingleColumn(SubWordType.Keyword_Haette);
      case SubWordType.Keyword_Wuerde:
        return buildSingleColumn(SubWordType.Keyword_Wuerde);
      default:
        return SizedBox();
    }

    return SizedBox();
  }

  Widget buildSingleColumn(SubWordType subWordType){
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 20,
          ),
          ...Pronoun.values
              .where((element) => element.isThirdPersonSingular)
              .map((pronoun) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Cell(Text(getText(pronoun)), alignment: Alignment.centerLeft,),
                Cell(WordUI.findBy(
                    wordType: WordType.Keyword,
                    subWordType: subWordType,
                    pronoun: pronoun)),
              ],
            );
          }).toList()
        ]);
  }

  static Widget buildSubType(SubWordType subWordType){
    return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Cell(null),
              Cell(Text("Past")),
              Cell(Text("Present"))
            ],
          ),
          ...Pronoun.values
              .where((element) => element.isThirdPersonSingular)
              .map((pronoun) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Cell(Text(getText(pronoun)), alignment: Alignment.centerLeft,),
                Cell(WordUI.findBy(
                    wordType: WordType.Keyword,
                    subWordType: subWordType,
                    pronoun: pronoun,
                    tense: Tense.Past)),
                Cell(WordUI.findBy(
                    wordType: WordType.Keyword,
                    subWordType: subWordType,
                    pronoun: pronoun,
                    tense: Tense.Present)),
              ],
            );
          }).toList()
        ]);
  }


}
