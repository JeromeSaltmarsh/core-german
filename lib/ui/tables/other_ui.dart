
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/model/repositories/memory_repository.dart';
import 'package:coregerman/ui/widgets/word_column.dart';
import 'package:flutter/material.dart';

class OtherUI extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return WordColumn(
      null,
      words: MemoryRepository.findWords(wordType: WordType.Unmapped),
    );
  }
}