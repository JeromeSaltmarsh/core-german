import 'package:coregerman/model/entities/sentence_value.dart';
import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/model/enums/gender.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/model/repositories/memory_repository.dart';
import 'package:coregerman/ui/configuration.dart';
import 'package:coregerman/ui/widgets/word_ui.dart';
import 'package:flutter/material.dart';
import 'package:coregerman/model/core_german.dart';

import 'package:coregerman/ui/global_utilities.dart';

class NounsUI extends StatelessWidget {

  double columnWidth = 140;
  double height = 600;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        buildColumn(Gender.Masculine),
        buildColumn(Gender.Feminine),
        buildColumn(Gender.Neutral),
      ],
    );
  }

  Widget buildColumn(Gender gender) {
    List<Word> words =
        MemoryRepository.findWords(wordType: WordType.Noun, gender: gender);

    return Container(
      child: Column(children: [
        DragTarget<SentenceValue>(
            onAccept: (sentenceValue){
              if(sentenceValue.wordId == null){
                Word word = coreGerman.addWord(sentenceValue.text, wordType: WordType.Noun, gender: gender);
                sentenceValue.wordId = word.id;
              }
            },
            builder: (context, candidateData, rejectedData) {
          return Container(
              width: columnWidth,
              child: Text(
                getText(gender),
                style: TextStyle(
                    color: UIConfiguration.defaultNotSelected),
              ));
        }),
        SizedBox(
          height: 5,
        ),
        Container(
          height: height,
          width: columnWidth,
          child: ListView.builder(
              itemCount: words.length,
              itemBuilder: (builderContext, index) {
                return WordUI(
                  words[index],
                );
              }),
        )
      ]),
    );
  }
}
