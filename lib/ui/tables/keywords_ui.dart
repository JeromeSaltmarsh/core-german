import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/model/enums/sub_word_type.dart';
import 'package:coregerman/model/repositories/memory_repository.dart';
import 'package:coregerman/ui/configuration.dart';
import 'package:coregerman/ui/core_german_ui.dart';
import 'package:coregerman/ui/global_utilities.dart';
import 'package:coregerman/ui/tables/subwordtype_ui.dart';
import 'package:coregerman/ui/widgets/word_ui.dart';
import 'package:flutter/material.dart';
import 'package:coregerman/model/core_german.dart';

class KeywordsUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: <Widget>[
          buildSubKeyWordsRow(context),
          SubWordTypeUI(),
        ],
      ),
    );
  }

  Widget buildSubKeyWordsRow(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        buildKeyword(SubWordType.Keyword_Haben, context),
        buildKeyword(SubWordType.Keyword_Sein, context),
        buildKeyword(SubWordType.Keyword_Werden, context),
        buildKeyword(SubWordType.Keyword_Machen, context),
        buildKeyword(SubWordType.Keyword_Waere, context),
        buildKeyword(SubWordType.Keyword_Haette, context),
        buildKeyword(SubWordType.Keyword_Wuerde, context),
        buildKeyword(SubWordType.Keyword_Gewesen, context),
      ],
    );
  }

  Widget buildKeyword(SubWordType subWordType, BuildContext context) {

    Word word = MemoryRepository.findWord(
        wordType: subWordType.wordType,
        subWordType: subWordType,
        condition: (word) => word.tense == null);

    bool selected = coreGerman.selectedSubWordType == subWordType;

    if (word != null) {
      if(word.value == null || word.value.isEmpty){
        coreGerman.deleteWord(word);
        return Text("Deleted");
      }

      return Container(
        padding: EdgeInsets.all(5),
          color: selected ? UIConfiguration.lightLightGrey : null,
          child: WordUI(word));
    }

    String text = getText(subWordType).replaceAll(
      "Keyword ",
      "",
    );

    return Container(
      color: selected ? UIConfiguration.lightLightGrey : null,
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.symmetric(vertical: 5),
      child: GestureDetector(
        onTap: () {
          showAddSubWordTypeWord(context, subWordType);
        },
        child: Text(
          text,
          style: TextStyle(
              color: selected
                  ? UIConfiguration.selectedTextColor
                  : UIConfiguration.unselectedTextColor),
        ),
      ),
    );
  }

  void showAddSubWordTypeWord(BuildContext context, SubWordType subWordType){
    showDialog(
        context: context,
        builder: (dialogContext) {
          TextEditingController germanController =
          TextEditingController();
          TextEditingController englishController =
          TextEditingController();
          return AlertDialog(
            title: Text(subWordType.toString()),
            content: Container(
              height: MediaQuery.of(context).size.height * GoldenRatioA,
              width: MediaQuery.of(dialogContext).size.width * GoldenRatio,
              child: Column(
                children: [
                  TextField(
                    decoration: InputDecoration(
                        labelText: "German"
                    ),
                    controller: germanController,
                    autofocus: true,
                  ),
                  TextField(
                    decoration: InputDecoration(
                        labelText: "English"
                    ),
                    controller: englishController,
                  )
                ],
              ),
            ),
            actions: [
              FlatButton(
                child: Text("Submit"),
                onPressed: () {
                  coreGerman.addWord(germanController.text,
                      wordType: subWordType.wordType,
                      subWordType: subWordType,
                      english: englishController.text);
                  Navigator.pop(dialogContext);
                },
              ),
            ],
          );
        });
  }
}
