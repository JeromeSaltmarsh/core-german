
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/model/repositories/memory_repository.dart';
import 'package:coregerman/ui/widgets/word_ui.dart';
import 'package:flutter/material.dart';

class AdjectivesUI extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: MemoryRepository.findWords(wordType: WordType.Adjective).map((adjective){
          return WordUI(adjective);
        }).toList(),
      ),
    );
  }

}