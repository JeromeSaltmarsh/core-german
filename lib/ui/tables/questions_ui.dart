import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/model/repositories/memory_repository.dart';
import 'package:coregerman/ui/widgets/word_ui.dart';
import 'package:flutter/material.dart';

class QuestionsUI extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: MemoryRepository.findWords(wordType: WordType.Question)
          .map((word) {
        return Padding(
          padding: const EdgeInsets.all(10.0),
          child: WordUI(word),
        );
      }).toList(),
    );
  }
}
