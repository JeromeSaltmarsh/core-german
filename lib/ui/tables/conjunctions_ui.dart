import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/model/repositories/memory_repository.dart';
import 'package:coregerman/ui/widgets/word_ui.dart';
import 'package:flutter/cupertino.dart';

class ConjuctionsUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<Word> conjuctions =
        MemoryRepository.findWords(wordType: WordType.Conjunction);

    return Column(
      children: conjuctions.map((word) => WordUI(word)).toList(),
    );
  }
}
