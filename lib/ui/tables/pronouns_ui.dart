import 'package:coregerman/model/enums/case.dart';
import 'package:coregerman/model/enums/pronouns.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/ui/widgets/cell.dart';
import 'package:coregerman/ui/widgets/word_ui.dart';
import 'package:flutter/material.dart';

import 'package:coregerman/ui/global_utilities.dart';

class PronounsUI extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        buildPronounRow(Case.Nominative),
        buildPronounRow(Case.Accusative),
        buildPronounRow(Case.Dative),
      ],
    );
  }

  Widget buildPronounRow(Case kase) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Cell(Text(getText(kase)), alignment: Alignment.centerLeft,),
          ...Pronoun.values
            .where((pronoun) => pronoun != Pronoun.Er_Sie_Es)
            .map((pronoun) {
          return Cell(WordUI.findBy(
              wordType: WordType.Pronoun, pronoun: pronoun, kase: kase));
        }).toList()]);
  }
}
