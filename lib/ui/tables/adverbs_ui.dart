
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/model/repositories/memory_repository.dart';
import 'package:coregerman/ui/widgets/word_ui.dart';
import 'package:flutter/material.dart';

class AdverbsUI extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Column(
      children: MemoryRepository.findWords(wordType: WordType.Adverb).map((word) => WordUI(word,)).toList(),
    );
  }
}