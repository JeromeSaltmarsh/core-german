import 'package:coregerman/model/enums/case.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/model/repositories/memory_repository.dart';
import 'package:coregerman/ui/widgets/word_column.dart';
import 'package:coregerman/ui/global_utilities.dart';
import 'package:flutter/material.dart';

class PrepositionsUI extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        buildColumn(Case.Accusative),
        buildColumn(Case.Dative),
        buildColumn(Case.Two_Way),
        buildColumn(Case.Genitive),
      ],
    );
  }

  Widget buildColumn(Case kase) {
    return WordColumn(
      getText(kase),
      words:
          MemoryRepository.findWords(wordType: WordType.Preposition, kase: kase)
              .toList(),
    );
  }
}
