
import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/ui/dialogs/add_word_dialog.dart';
import 'package:flutter/material.dart';

Future<Word> showAddWordDialog(BuildContext context, {String value, WordType wordType}) {
  return buildDialog<Word>(context, AddWordDialog(initialValue: value, wordType: wordType,));
}

Future<T> buildDialog<T>(BuildContext context, Widget content){
  return showDialog<T>(
      context: context,
      builder: (dialogContext) {
        return content;
      });
}