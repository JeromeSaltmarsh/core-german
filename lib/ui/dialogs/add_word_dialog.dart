import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/model/enums/case.dart';
import 'package:coregerman/model/enums/difficulty.dart';
import 'package:coregerman/model/enums/gender.dart';
import 'package:coregerman/model/enums/modal_verbs.dart';
import 'package:coregerman/model/enums/pronouns.dart';
import 'package:coregerman/model/enums/sub_word_type.dart';
import 'package:coregerman/model/enums/tense.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/ui/global_utilities.dart';
import 'package:coregerman/ui/widgets/simple_drop_down.dart';
import 'package:flutter/material.dart';
import 'package:coregerman/model/core_german.dart';

class AddWordDialog extends StatefulWidget {
  final String initialValue;
  final WordType wordType;
  final Gender gender;
  final Case kase;
  final ModalVerb modalVerb;
  final Pronoun pronoun;
  final Tense tense;
  final SubWordType subWordType;

  AddWordDialog(
      {this.initialValue = "",
      this.wordType = WordType.Definite_Article,
      this.gender = Gender.Masculine,
      this.kase = Case.Nominative,
      this.modalVerb,
      this.pronoun,
      this.tense,
      this.subWordType});

  @override
  _AddWordDialogState createState() => _AddWordDialogState();
}

class _AddWordDialogState extends State<AddWordDialog> {
  TextEditingController valueController;
  TextEditingController englishController;
  WordType wordType;
  Gender gender;
  Case kase;
  ModalVerb modalVerb;
  Pronoun pronoun;
  Tense tense;
  Difficulty difficulty = Difficulty.Beginner;
  SubWordType subWordType;

  @override
  void initState() {
    super.initState();
    wordType = widget.wordType;
    gender = widget.gender;
    kase = widget.kase;
    modalVerb = widget.modalVerb;
    pronoun = widget.pronoun;
    tense = widget.tense;
    subWordType = widget.subWordType;

    if(subWordType != null && subWordType.isModalVerb){
      modalVerb = subWordType.modalVerb;
    }

    valueController = TextEditingController(text: widget.initialValue);
    englishController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    print("AddWordDialog.build()");

    double dialogWidth = MediaQuery.of(context).size.width * GoldenRatioA;
    double dialogHeight = MediaQuery.of(context).size.height * GoldenRatio;
    double columnWidth = dialogWidth * GoldenRatioA;

    return AlertDialog(
      content: Container(
        width: dialogHeight,
        height: dialogWidth,
        alignment: Alignment.center,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              ListTile(
                leading: Container(width: columnWidth, child: Text("Value")),
                title: TextField(
                  controller: valueController,
                ),
              ),
              ListTile(
                leading: Container(
                    width: columnWidth, child: Text("English Translation")),
                title: TextField(
                  controller: englishController,
                ),
              ),
              ListTile(
                leading:
                    Container(width: columnWidth, child: Text("Difficulty")),
                title: SimpleDropDown<Difficulty>(
                  Difficulty.values,
                  onChanged: (value) {
                    setState(() {
                      difficulty = value;
                    });
                  },
                  initialValue: difficulty,
                ),
              ),
              ListTile(
                leading: Container(width: columnWidth, child: Text("Type")),
                title: SimpleDropDown<WordType>(
                  WordType.values,
                  initialValue: widget.wordType,
                  onChanged: (value) {
                    setState(() {
                      wordType = value;
                    });
                  },
                ),
              ),
              if (wordType.hasSubWordType)
                ListTile(
                  leading:
                      Container(width: columnWidth, child: Text("Sub Type")),
                  title: SimpleDropDown<SubWordType>(
                    SubWordType.values,
                    onChanged: (value) {
                      setState(() {
                        subWordType = value;
                      });
                    },
                    initialValue: widget.subWordType,
                  ),
                ),
              if (wordType.hasGender)
                ListTile(
                  leading: Container(width: columnWidth, child: Text("Gender")),
                  title: SimpleDropDown<Gender>(
                    Gender.values,
                    onChanged: (value) {
                      setState(() {
                        gender = value;
                      });
                    },
                    initialValue: widget.gender,
                  ),
                ),
              if (wordType.hasCase)
                ListTile(
                  leading: Container(width: columnWidth, child: Text("Case")),
                  title: SimpleDropDown<Case>(
                    Case.values,
                    onChanged: (value) {
                      setState(() {
                        kase = value;
                      });
                    },
                    initialValue: widget.kase,
                  ),
                ),
              if (wordType.hasModalVerb)
                ListTile(
                  leading:
                      Container(width: columnWidth, child: Text("Perfect")),
                  title: SimpleDropDown<ModalVerb>(
                    ModalVerb.values,
                    onChanged: (value) {
                      setState(() {
                        modalVerb = value;
                      });
                    },
                    initialValue: widget.modalVerb,
                  ),
                ),
              if (wordType.hasPronoun)
                ListTile(
                  leading: Container(
                      width: columnWidth, child: Text("Verb Pronoun")),
                  title: SimpleDropDown<Pronoun>(
                    Pronoun.values,
                    onChanged: (value) {
                      setState(() {
                        pronoun = value;
                      });
                    },
                    initialValue: widget.pronoun,
                  ),
                ),
              if (wordType.hasTense && (subWordType == null || subWordType.hasTense))
                ListTile(
                  leading: Container(width: columnWidth, child: Text("Tense")),
                  title: SimpleDropDown<Tense>(
                    Tense.values,
                    onChanged: (value) {
                      setState(() {
                        tense = value;
                      });
                    },
                    initialValue: widget.tense,
                  ),
                ),
            ],
          ),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text("Cancel"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        FlatButton(child: Text("Submit"), onPressed: submit)
      ],
    );
  }

  void submit() {
    String value = valueController.text;
    String englishValue = englishController.text;

    print("AddWordDialog.submit(value: $value, wordType: $wordType, gender: $gender, case: $kase, english: $englishValue, modalVerb: $modalVerb, pronoun: $pronoun, tense: $tense, difficulty: $difficulty})");

    Word word = coreGerman.addWord(value,
        wordType: wordType,
        gender: gender,
        kase: kase,
        english: englishValue,
        modalVerb: modalVerb,
        pronoun: pronoun,
        tense: tense,
        difficulty: difficulty,
        subWordType: subWordType);

    coreGerman.refresh();
    Navigator.pop(context, word);
  }
}
