import 'package:coregerman/model/entities/sentence.dart';
import 'package:coregerman/model/repositories/memory_repository.dart';
import 'package:coregerman/ui/configuration.dart';
import 'package:coregerman/ui/global_utilities.dart';
import 'package:flutter/material.dart';
import 'package:coregerman/model/core_german.dart';

class SearchSentencesDialog extends StatefulWidget {
  @override
  _SearchSentencesDialogState createState() => _SearchSentencesDialogState();
}

class _SearchSentencesDialogState extends State<SearchSentencesDialog> {
  TextEditingController searchController;
  List<Sentence> results = [];

  @override
  void initState() {
    super.initState();
    searchController = TextEditingController();
    results = MemoryRepository.sentences;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("How would you say?"),
      actions: <Widget>[
        FlatButton(
          child: Text(
            "Cancel",
            style: TextStyle(color: Colors.grey),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ],
      content: Container(
        width: MediaQuery.of(context).size.width * GoldenRatio,
        child: Column(
          children: <Widget>[
            TextField(
              autofocus: true,
              controller: searchController,
              onChanged: (value) {

                results = MemoryRepository.searchSentences(value);
                print("results found ${results.length}");

                setState(() {

                });
              },
            ),
            if (results.isNotEmpty)
              Container(
                  height: MediaQuery.of(context).size.height * GoldenRatio,
                  child: ListView.builder(
                      itemCount: results.length,
                      itemBuilder: (builderContext, index) {
                        Sentence sentence = results[index];

                        return ListTile(
                          onTap: () {
                            coreGerman.selectSentence(sentence);
                            Navigator.pop(context);
                          },
                          title: renderSentence(sentence),
                          trailing: coreGerman.administrator ? IconButton(
                            icon: Icon(Icons.delete, color: Colors.red,),
                            onPressed: (){
                              coreGerman.deleteSentence(sentence);
                              setState(() {
                                results.remove(sentence);
                              });
                            },
                          ) : null,
                          subtitle: GestureDetector(
                            child: Text(sentence.english != null && sentence.english.isNotEmpty ? sentence.english : "Translate"),
                            onTap: () async {
                              await showDialog(
                                  context: context,
                                  builder: (dialogContext) {
                                    String english = "";

                                    return AlertDialog(
                                      content: Container(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                GoldenRatioA,
                                        child: TextField(
                                          autofocus: true,
                                          onChanged: (value) {
                                            english = value;
                                          },
                                        ),
                                      ),
                                      actions: [
                                        FlatButton(
                                          child: Text("Cancel"),
                                          onPressed: () {
                                            Navigator.pop(dialogContext);
                                          },
                                        ),
                                        FlatButton(
                                          child: Text("Submit"),
                                          onPressed: () {
                                            sentence.english = english;
                                            sentence.save();
                                            coreGerman.refresh();
                                            Navigator.pop(dialogContext);
                                          },
                                        ),
                                      ],
                                    );
                                  });
                              setState(() {

                              });
                            },
                          ),
//                          trailing: IconButton(icon: Icon(Icons.add, color: Colors.blue,), onPressed: (){
//                            coreGerman.createAnswerTo(question);
//                          },),
                        );
                      }))
          ],
        ),
      ),
    );
  }

  Widget renderSentence(Sentence sentence) {
    if (sentence == null) {
      return Text("No sentence found");
    }

    return Row(
      children: sentence.sentenceValues
          .map((e) => Container(
              margin: UIConfiguration.wordMargin,
              child: Text(
                e.text,
                style: TextStyle(color: Colors.blue),
              )))
          .toList(),
    );
  }
}
