import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/model/enums/case.dart';
import 'package:coregerman/model/enums/difficulty.dart';
import 'package:coregerman/model/enums/gender.dart';
import 'package:coregerman/model/enums/modal_verbs.dart';
import 'package:coregerman/model/enums/pronouns.dart';
import 'package:coregerman/model/enums/sub_word_type.dart';
import 'package:coregerman/model/enums/tense.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/ui/widgets/simple_drop_down.dart';
import 'package:flutter/material.dart';
import 'package:coregerman/model/core_german.dart';

class EditWordDialog extends StatefulWidget {

  final Word word;

  EditWordDialog(this.word);

  @override
  _EditWordDialogState createState() => _EditWordDialogState();
}

class _EditWordDialogState extends State<EditWordDialog> {

  TextEditingController valueController;
  TextEditingController englishController;
  double dialogWidth = 500;

  Word get word => widget.word;

  @override
  void initState() {
    super.initState();
    Word word = widget.word;
    valueController = TextEditingController(text: word.value);
    englishController = TextEditingController(text: word.english);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("Edit Word"),
      content: Container(
        width: dialogWidth,
        height: 400,
        alignment: Alignment.center,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              ListTile(
                leading: text("Value"),
                title: TextField(
                  controller: valueController,
                  onChanged: (value){
                    word.value = value;
                  },
                ),
              ),
              ListTile(
                leading: text("English Translation"),
                title: TextField(
                  controller: englishController,
                  onChanged: (value){
                    word.english = value;
                  },
                ),
              ),
              ListTile(
                leading: text("Difficulty"),
                title: SimpleDropDown<Difficulty>(
                  Difficulty.values,
                  onChanged: (value) {
                    setState(() {
                      word.difficulty = value;
                    });
                  },
                  initialValue: word.difficulty,
                ),
              ),
              ListTile(
                leading: text("Type"),
                title: SimpleDropDown<WordType>(
                  WordType.values,
                  onChanged: (value) {
                    setState(() {
                      word.type = value;
                    });
                  },
                  initialValue: widget.word.type,
                ),
              ),
              if (word.type.hasSubWordType)
                ListTile(
                  leading:
                  Container(child: Text("Sub Type")),
                  title: SimpleDropDown<SubWordType>(
                    SubWordType.values,
                    onChanged: (value) {
                      setState(() {
                        word.subWordType = value;
                      });
                    },
                    initialValue: word.subWordType,
                  ),
                ),
              if (word.type.hasGender)
                ListTile(
                  leading: text("Gender"),
                  title: SimpleDropDown<Gender>(
                    Gender.values,
                    onChanged: (value) {
                      setState(() {
                        word.gender = value;
                      });
                    },
                    initialValue: widget.word.gender,
                  ),
                ),
              if (word.type.hasCase)
                ListTile(
                  leading: text("Case"),
                  title: SimpleDropDown<Case>(
                    Case.values,
                    onChanged: (value) {
                      setState(() {
                        word.kase = value;
                      });
                    },
                    initialValue: widget.word.kase,
                  ),
                ),
              if (word.type.hasModalVerb)
                ListTile(
                  leading: text("Perfect"),
                  title: SimpleDropDown<ModalVerb>(
                    ModalVerb.values,
                    onChanged: (value) {
                      setState(() {
                        word.modalVerb = value;
                      });
                    },
                    initialValue: widget.word.modalVerb,
                  ),
                ),
              if (word.type.hasPronoun)
                ListTile(
                  leading: text("Verb Pronoun"),
                  title: SimpleDropDown<Pronoun>(
                    Pronoun.values,
                    onChanged: (value) {
                      setState(() {
                        word.pronoun = value;
                      });
                    },
                    initialValue: widget.word.pronoun,
                  ),
                ),
              if (word.type.hasTense)
                ListTile(
                  leading: text("Tense"),
                  title: SimpleDropDown<Tense>(
                    Tense.values,
                    onChanged: (value) {
                      setState(() {
                        word.tense = value;
                      });
                    },
                    initialValue: widget.word.tense,
                  ),
                ),
            ],
          ),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text("Cancel"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        FlatButton(
          child: Text("Delete"),
          onPressed: () {
            coreGerman.deleteWord(widget.word);
            Navigator.pop(context);
          },
        ),
        FlatButton(child: Text("Save"), onPressed: submit)
      ],
    );
  }

  void submit() {
    word.save();
    coreGerman.refresh();
    Navigator.pop(context);
  }

  Widget text(String value) {
    return Container(width: dialogWidth / 3, child: Text(value));
  }
}
