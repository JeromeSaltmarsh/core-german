
import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/model/repositories/memory_repository.dart';
import 'package:coregerman/ui/ui_extensions.dart';
import 'package:flutter/material.dart';

class SearchWordDialog extends StatefulWidget{

  final String text;

  SearchWordDialog(this.text);

  @override
  _SearchWordDialogState createState() => _SearchWordDialogState();
}

class _SearchWordDialogState extends State<SearchWordDialog> {

  TextEditingController controller;
  FocusNode textFieldFocusNode;
  List<Word> searchResults;

  @override
  void initState() {
    super.initState();
    textFieldFocusNode = FocusNode();
    controller = TextEditingController(text: widget.text);
    searchResults = MemoryRepository.searchWords(widget.text);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Container(
        height: 500,
        width: 600,
        child: Column(
          children: <Widget>[
            Container(
              width: 200,
              child: TextField(
                autofocus: true,
                controller: controller,
                focusNode: textFieldFocusNode,
                onChanged: (value) async {
                  setState(() {
                    searchResults = MemoryRepository.searchWords(controller.text);
                  });
                },
              ),
            ),
            Container(
              height: 200,
              child: ListView.builder(
                  itemCount: searchResults.length,
                  itemBuilder: (builderContext, index) {
                    Word word = searchResults[index];

                    return ListTile(
                      leading: Text(
                        word.value,
                        style: TextStyle(color: word.gender.color),
                      ),
                      title: Text(word.type.toString()),
                      trailing:
                      word.kase != null ? Text(word.kase.toString()) : null,
                      subtitle: word.gender != null
                          ? Text(word.gender.toString())
                          : null,
                      onTap: () {
                          Navigator.pop(context, word);
                      },
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }
}