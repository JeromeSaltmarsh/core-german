
import 'package:flutter/material.dart';

String getText(dynamic value, {bool replaceUmlauts = true}) {
  String valueString = value.toString().replaceAll("_", " ");
  int indexOfDot = valueString.indexOf(".");
  if (indexOfDot == -1) {
    return valueString;
  }
  String text = valueString.substring(indexOfDot + 1, valueString.length);

  if (replaceUmlauts) {
    text = text.replaceAll("oe", UmlautO);
    text = text.replaceAll("ae", UmlautA);
    text = text.replaceAll("ue", UmlautU);
  }
  return text;
}

String UmlautA = "ä";
String UmlautO = "ö";
String UmlautU = "ü";

const double GoldenRatio = 0.61803398873;
const double GoldenRatioA = 1 - GoldenRatio;
const SelectedBorderRadius = BorderRadius.all(const Radius.circular(4));

BoxDecoration borderDecoration(Color color) {
  return BoxDecoration(
      border: Border.all(color: color, width: 1, style: BorderStyle.solid),
      borderRadius: SelectedBorderRadius);
}
