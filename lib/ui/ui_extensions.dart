import 'package:coregerman/model/core_german.dart';
import 'package:coregerman/model/entities/word.dart';
import 'package:coregerman/model/enums/case.dart';
import 'package:coregerman/model/enums/gender.dart';
import 'package:coregerman/model/enums/modal_verbs.dart';
import 'package:coregerman/model/enums/pronouns.dart';
import 'package:coregerman/model/enums/sub_word_type.dart';
import 'package:coregerman/model/enums/tense.dart';
import 'package:coregerman/model/enums/word_type.dart';
import 'package:coregerman/ui/configuration.dart';
import 'package:coregerman/ui/dialogs/add_word_dialog.dart';
import 'package:coregerman/ui/global_utilities.dart';
import 'package:flutter/material.dart';

extension GenderExtensions on Gender {
  Color get color {
    if (this == null) {
      return UIConfiguration.defaultColor;
    }
    switch (this) {
      case Gender.Masculine:
        return UIConfiguration.masculineColor;
      case Gender.Feminine:
        return UIConfiguration.feminineColor;
      case Gender.Neutral:
        return UIConfiguration.neutralColor;
      case Gender.Plural:
        return UIConfiguration.pluralColor;
    }
    return UIConfiguration.defaultColor;
  }
}

extension WordExtensions on Word{
  Color get color{
    if(this == null){
      return UIConfiguration.defaultColor;
    }

    if(this.type.hasGender){
      return this.gender.color;
    }
    return UIConfiguration.defaultColor;
  }
}

extension BuildContextExtensions on BuildContext {

  Future showAddSubWordTypeWord(SubWordType subWordType) {
    return showDialog(
        context: this,
        builder: (dialogContext) {
          TextEditingController germanController = TextEditingController();
          TextEditingController englishController = TextEditingController();
          return AlertDialog(
            title: Text(subWordType.toString()),
            content: Container(
              height: MediaQuery.of(this).size.height * GoldenRatioA,
              width: MediaQuery.of(dialogContext).size.width * GoldenRatio,
              child: Column(
                children: [
                  TextField(
                    decoration: InputDecoration(labelText: "German"),
                    controller: germanController,
                    autofocus: true,
                  ),
                  TextField(
                    decoration: InputDecoration(labelText: "English"),
                    controller: englishController,
                  )
                ],
              ),
            ),
            actions: [
              FlatButton(
                child: Text("Submit"),
                onPressed: () {
                  coreGerman.addWord(germanController.text,
                      wordType: subWordType.wordType,
                      subWordType: subWordType,
                      english: englishController.text);
                  Navigator.pop(dialogContext);
                },
              ),
            ],
          );
        });
  }

  Future<Word> showAddWordDialog({
    String initialValue = "",
    WordType wordType = WordType.Definite_Article,
    Gender gender = Gender.Masculine,
    Case kase = Case.Nominative,
    ModalVerb modalVerb,
    Pronoun pronoun,
    Tense tense,
    SubWordType subWordType,
  }) {
    return buildDialog(
        AddWordDialog(
          initialValue: initialValue,
          wordType: wordType,
          gender: gender,
          kase: kase,
          modalVerb: modalVerb,
          pronoun: pronoun,
          tense: tense,
          subWordType: subWordType,
        ));
  }

  Future<T> buildDialog<T>(Widget content) {
    return showDialog<T>(
        context: this,
        builder: (dialogContext) {
          return content;
        });
  }


}