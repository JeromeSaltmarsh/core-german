

TODO
- Fix Modal Verbs
- Mobile Friendly UI
- English to German Language Settings
- Previous Sentence Button
- Possessive pronouns
- warum, weil, wegen, vielleicht, dass


Completed
- Finish Keywords
- Fix question mark issue
- Enter German Sentence
- Fix Drag and Drop
- Edit Sentence
